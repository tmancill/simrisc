#ifndef INCLUDED_ANALYSIS_
#define INCLUDED_ANALYSIS_

#include <iosfwd>

#include "../parser/parser.h"
#include "../typedefs/typedefs.h"

class Analysis
{
    StringVect d_labels;

    public:
                                    // stream: the analysis: secifications
        Analysis(std::istream &&stream, uint16_t lineNr);
        void run();

    private:
        void actualize(Parser::OptionsVect const &specs);
        void requireBase() const;
};

#endif




