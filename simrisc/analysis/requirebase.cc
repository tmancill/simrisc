//#define XERR
#include "analysis.ih"

void Analysis::requireBase() const
{
    error_code ec;

    auto const &base = Options::instance().base();

    if (
        not filesystem::create_directory(base, ec)
        and 
        ec.value() != 0
    )
        throw Exception{} << "Cannot create base directory " << base;
}
