//#define XERR
#include "analysis.ih"

    // the error count is reset by the Simulator

Analysis::Analysis(std::istream &&stream, uint16_t lineNr)
{
    Parser parser;
    Parser::OptionsVect options;

    parser.load(stream, lineNr, d_labels, options); // read the analysis specs

    actualize(options);                          

    parser.load(Options::instance().configFile());  // read the config file
}
