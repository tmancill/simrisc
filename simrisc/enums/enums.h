#ifndef INCLUDED_ENUMS_
#define INCLUDED_ENUMS_

enum Constants
{
    N_BIRADS = 4,   // 4 bi-rad categories (cf. tech man. section Densities)
    MAX_VARY_TRIES = 10, // max #attempts to obtain a valid varied value

    END_AGE         = 101,
    MAX_AGE         = END_AGE - 1,
};

enum VaryType
{
    VARY_OK,                    // every value is OK
    VARY_POS,                   // varied values must be >= 0
    VARY_PROB                   // varied values must be probabilities
};

enum DistType                   // update Random::Sizes, Dostribution::s_name
{                               // when modified

    UNIFORM,                    // always used distributions
    UNIFORM_CASE,
    LOGNORMAL,

    N_STD_DISTRIBUTIONS,
 
    NORMAL_VARY = N_STD_DISTRIBUTIONS,

    UNIFORM_VARY,
    LOGNORMAL_VARY,

    N_DISTRIBUTIONS
};

enum GeneratorType
{
    FIXED_SEED,
    INCREASING_SEED,
    RANDOM_SEED,
};

enum ParamsSrc         
{
    CONFIGFILE,
    ANALYSIS,
    NONE,
};


#endif
