//#define XERR
#include "main.ih"

                            // update Options::alter, Options::inspect and
                            // Options::Options if g_longOpts is modified
Arg::LongOption g_longOpts[] =
{
    Arg::LongOption{"base",         'B'},
    Arg::LongOption{"config",       'C'},
    Arg::LongOption{"data",         'D'},
    Arg::LongOption{"death-age",    'a'},
    Arg::LongOption{"help",         'h'},
    Arg::LongOption{"last-case",    'l'},
    Arg::LongOption{"one-analysis", 'o'},
    Arg::LongOption{"parameters",   'P'},
    Arg::LongOption{"rounds",       'R'},
    Arg::LongOption{"sensitivity",  'S'},
    Arg::LongOption{"tumor-age",    't'},
    Arg::LongOption{"verbose",      'V'},
    Arg::LongOption{"version",      'v'},
};

auto const *g_longEnd = g_longOpts + size(g_longOpts);

//main
int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("a:B:l:C:D:hoP:R:S:t:vV",
                                      g_longOpts, g_longEnd, argc, argv);
    arg.versionHelp(usage, Icmake::version, arg.option('o') ? 0 : 1);

    Options::instance();            // construct the Options object

    Simulator simulator;            // construct the simulator
    simulator.run();                // and run it.
}
//=
catch(int x)
{
    return Arg::instance().option("hv") ? 0 : x;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}

catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
