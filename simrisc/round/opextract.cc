//#define XERR
#include "round.ih"

std::istream &operator>>(std::istream &in, Round &round)
{
    string specification;
    in >> specification;

    if (specification == "none")
        round.d_age = END_AGE;
    else
    {
        istringstream inStr{ specification };
        if (not (inStr >> round.d_age))
            in.setstate(ios::failbit);
    }

    return in;
}

