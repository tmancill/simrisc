//#define XERR
#include "round.ih"

bool Round::add(uint16_t idx)
{
    if 
    (
        find(d_modalities.begin(), d_modalities.end(), idx) 
        != 
        d_modalities.end()
    )
        return false;

    d_modalities.push_back(idx);
    return true;
}
