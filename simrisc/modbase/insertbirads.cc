//#define XERR
#include "modbase.ih"

// static
void ModBase::insertBirads(ostream &out, char const *label, 
                           double const *value)
{
    out << "    bi-rad " << label;

    for (size_t idx = 0; idx != N_BIRADS; ++idx)
        out << static_cast<char>('a' + idx) << ": " << setw(5) << 
                                                        value[idx] << ", ";

    out.put('\n');
}
