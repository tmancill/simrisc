//#define XERR
#include "modbase.ih"

// static
void ModBase::insertSpecificity(ostream &out, unsigned fill, 
                                SpecificityVect const &specVect)
{
    outSpec(out, "    specificity:", fill, specVect.front());

    for_each(specVect.begin() + 1, specVect.end(), 
        [&](auto const &spec)
        {
            outSpec(out, "                ", fill, spec);
        }
    );
}
