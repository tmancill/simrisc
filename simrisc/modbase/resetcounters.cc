//#define XERR
#include "modbase.ih"

void ModBase::resetCounters(size_t nRounds)
{
    d_count = SizeVect(nRounds);
    d_falsePositives = 0;
}

