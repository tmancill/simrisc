//#define XERR
#include "modbase.ih"

std::ostream &operator<<(std::ostream &out, ModBase const &modBase)
{
    out << "  " << modBase.id() << ":\n" <<
           "    costs:        " << modBase.d_cost << '\n';

    modBase.vInsert(out);

    return out.put('\n');
}
