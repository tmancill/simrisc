//#define XERR
#include "modbase.ih"

void ModBase::costHandler(StringVect &keywords)
{
    keywords.back() = "costs:";
    Parser::nonNegative(keywords, d_cost);
}
