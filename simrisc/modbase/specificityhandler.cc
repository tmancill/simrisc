//#define XERR
#include "modbase.ih"

void ModBase::specificityHandler(SpecificityVect &dest, StringVect &keywords)
{
    keywords.back() = "specificity:";

    auto lines = Parser::one(keywords);

    if (not lines)                              // keywords not found
        return;

    istringstream in{ lines.get()->tail };       // prepare for extraction
    Specificity spec;

    unsigned count = 0;
    unsigned success = 0;

    while (in >> spec)                   // extract the next specificity
    {
        ++count;        

        if 
        (
            not spec.proportion()
            or
            not spec.ageGroup().nextRange(dest)
        )
            break;

        dest.push_back(spec);
        ++success;
    }

    if (success == 0 or count != success)
        Err::specification();
}


