//#define XERR
#include "modbase.ih"

void ModBase::doubleHandler(double *dest, StringVect const &keywords,
                           bool  (*proportionCheck)
                                 (double const *, unsigned))
{
    if (
        Parser::extract( Parser::one(keywords), dest, N_BIRADS)
        and 
        proportionCheck
        and 
        not (*proportionCheck)(dest, N_BIRADS)
    )
        Err::range();
}



