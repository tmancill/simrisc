#ifndef INCLUDED_SURVIVAL_
#define INCLUDED_SURVIVAL_

// Tumor:
//     Survival:
//         #              value        spread      dist:
//         type:  a        .00004475   .000004392  Normal  
//         type:  b       1.85867      .0420       Normal
//         type:  c      - .271        .0101       Normal
//         type:  d       2.0167       .0366       Normal

#include "../vsd/vsd.h"

class Survival
{
    VSDvect d_vsd;

    public:
        Survival();

        void vary();
        VSD const &operator[](size_t idx) const;

        void writeParameters(std::ostream &out) const;
};
        
inline VSD const &Survival::operator[](size_t idx) const
{
    return d_vsd[idx];
}

#endif


