#define XERR
#include "survival.ih"

Survival::Survival()
:
    d_vsd(4)
{
    auto lines = Parser::any( { "Tumor:", "Survival:", "type:" } );

    while (true)
    {
        auto const *line = lines.get();
        if (not line)
            break;

        VSD vsd;
        char type;
        vsd.setMode(VSD::MEAN);
        if (not Parser::extract(*line, type, vsd))  
            continue;

        if (not ('a' <= type and type <= 'd'))
        {
            Err::msg(Err::INVALID_TYPE) << '`' << type << '\'' << endl;
            continue;
        }

        d_vsd[type - 'a'] = vsd;
    }

    for (auto const &vsd: d_vsd)
    {
        if (vsd.distType() == N_DISTRIBUTIONS)
        {
            Err::msg(Err::MISSING_TYPE) << '\n';
            break;
        }
    }
}
