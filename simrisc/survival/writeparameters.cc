//#define XERR
#include "survival.ih"

void Survival::writeParameters(ostream &out) const
{
    Globals::setPrecision(out, 5) <<  "    Survival:\n";

    char type = 'a';
    for (auto const &vsd: d_vsd)
    {
        out <<  setw(6) << ' ' << "type: " << type << "   " << vsd << '\n';
        ++type;
    }

    out.put('\n');
}
