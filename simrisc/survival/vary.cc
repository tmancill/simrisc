//#define XERR
#include "survival.ih"

void Survival::vary()
{
    for (auto &vsd: d_vsd)
        vsd.vary();

    // the default Distribution setting is VARY_OK: OK for Survival
}
