//#define XERR
#include "err.ih"

                                // line context available
ostream &Err::msg(Context err)
{
    if (not s_handle)
        return imsg;            // switched 'off'

    s_handle = false;
    return emsg << s_src[s_lineInfo->src] << " [" << 
                         s_lineInfo->lineNr << "]: " << 
                         s_context[err] << ": ";
}
