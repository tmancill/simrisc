//#define XERR
#include "err.ih"

ostream &Err::msg(Plain plain)              // no line context
{
    return emsg << s_plain[plain];
}
