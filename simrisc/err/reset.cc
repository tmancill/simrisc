#define XERR
#include "err.ih"

// static 
LineInfo const * Err::reset(LineInfo const &lineInfo)
{
    s_lineInfo = &lineInfo;
    s_handle = true;

    return s_lineInfo;
}
