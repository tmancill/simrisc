//#define XERR
#include "globals.ih"

// static
double Globals::findAge(DoubleVect const &cumProb, double prob)
{
    auto begin = cumProb.begin();
    auto last = upper_bound(begin, cumProb.end(), prob);

    auto first = lower_bound(begin, last, *(last - 1));

    return (first - begin) +
            (                           // interpolate 'prob' in the range of
                (last - first) *        // categories containing 'prob'
                    (prob - *first) /   
                    ((last == cumProb.end() ? 1.0 : *last)  - *first)
            );
}








