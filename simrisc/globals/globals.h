#ifndef INCLUDED_GLOBALS_
#define INCLUDED_GLOBALS_

#include <ostream>
#include <iomanip>

#include "../typedefs/typedefs.h"

struct Globals
{
    static constexpr double TOLERANCE = 1e-8;
    static constexpr double WEAK_TOLERANCE = 1e-3; 
    static constexpr double NO_TUMOR = -1;          // tumor/

    static double const s_sqrt2PI;

    static bool proportions(double const *values, unsigned count = 1);
    static bool proportion(double const &value);

    static double findAge(DoubleVect const &cumProb, double prob);

    static bool isZero(double value);
    static bool isPositiveZero(double value);
                                                // -1: lhs < rhs
                                                //  0: lhs == rhs
                                                //  1: lhs > rhs
    static int weakCompare(double lhs, double rhs); // uses WEAK_TOLERANCE

    static std::ostream &setPrecision(std::ostream &out, uint16_t nDigits);
};

// static
inline bool Globals::isPositiveZero(double value)
{
    return 0 <= value and value <= TOLERANCE;
}

// static 
inline bool Globals::proportion(double const &value)
{
    return proportions(&value, 1);
}

#endif
