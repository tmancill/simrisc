//#define XERR
#include "globals.ih"

// static
bool Globals::proportions(double const *values, unsigned count)
{
    return 
        find_if(values, values + count, 
            [&](double value)
            {
                return value < 0 or 1 < value;
            }
        ) 
        == values + count;
}
