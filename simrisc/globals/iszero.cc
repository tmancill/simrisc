//#define XERR
#include "globals.ih"

// static
bool Globals::isZero(double value)
{
    return fabs(value) <= TOLERANCE;
}

