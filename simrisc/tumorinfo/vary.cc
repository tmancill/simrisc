//#define XERR
#include "tumorinfo.ih"

void TumorInfo::vary()
{
    d_incidence.vary();
    d_growth.vary();
    d_survival.vary();
    d_beir7.vary();
}
