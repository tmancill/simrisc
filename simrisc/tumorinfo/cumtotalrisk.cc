#define XERR
#include "tumorinfo.ih"

    // see void calcTotalRisk(int carrierIndex)

void TumorInfo::cumTotalRisk(DoubleVect const &radiationRisk)
{
    double sum = 0;
    d_cumRisk.clear();

    uint16_t carrierIdx = d_incidence.carrier();

    auto iter = radiationRisk.begin();

    for (double risk: d_incidence.tumorRisk(carrierIdx))
        d_cumRisk.push_back(sum += risk * *iter++);
}


//void calcTotalRisk(int carrierIndex) {
//    float totRisk;
//    for (int T = 0; T < 101; T++) {
//        totRisk = radRisk[T] * tumorRisk[carrierIndex][T];  <<------ !!!
//        if (T == 0) {
//            cumTotRisk[T] = totRisk;
//        }
//        else {
//            cumTotRisk[T] = cumTotRisk[T-1] + totRisk;
//        }
//    }
//
