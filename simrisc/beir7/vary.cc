#define XERR
#include "beir7.ih"

void Beir7::vary()
{
    d_beta = d_vsd.distribution().vary(d_orgBeta);
    d_vsd.vary();
}
