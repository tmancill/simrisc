#define XERR
#include "beir7.ih"

//Tumor:
//    #       beta    eta     spread  dist. 
//    beir7:  0.51    -2.0    0.32    Normal  

Beir7::Beir7()
{
    d_vsd.setMode(VSD::MEAN);
    Parser::extract( Parser::one({ "Tumor:", "beir7:" }), d_beta, d_vsd);
    d_orgBeta = d_beta;                     // used for resetting

    // the default Distribution setting is VARY_OK: OK for Beir7
}



