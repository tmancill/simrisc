#ifndef INCLUDED_BEIR7_
#define INCLUDED_BEIR7_

//Tumor:
//        #     beta    eta     spread      dist
//    Beir7:    .51     -2.0    .32         Normal  

#include <iosfwd>

#include "../typedefs/typedefs.h"
#include "../vsd/vsd.h"

class Beir7
{
    double d_beta;
    double d_orgBeta;

    VSD d_vsd;                  // contains eta and spread/distribution

    public:
        Beir7();

        double beta() const;
        double eta() const;
        double spread() const;
        DistType distType() const;

        void vary();

        void writeParameters(std::ostream &out) const;
};

inline double Beir7::beta() const
{
    return d_beta;
}

inline double Beir7::eta() const
{
    return d_vsd.value();
}

inline double Beir7::spread() const
{
    return d_vsd.spread();
}

inline DistType Beir7::distType() const
{
    return d_vsd.distType();
}

#endif

