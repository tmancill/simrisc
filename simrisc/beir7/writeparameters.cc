//#define XERR
#include "beir7.ih"

void Beir7::writeParameters(ostream &out) const
{
    Globals::setPrecision(out, 2) <<
        "  Beir7:\n"
        "    beta:         " << setw(8) << d_beta << "\n"
        "    eta:          " << setw(8) << eta() << "\n"
        "    spread:       " << setw(8) << spread() << "\n"
        "    distribution: " << setw(8) << d_vsd.distName() << "\n"
        "\n";
}
