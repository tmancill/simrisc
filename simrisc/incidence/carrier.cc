//#define XERR
#include "incidence.ih"

uint16_t Incidence::carrier() const
{
    double value = Random::instance().uniform();

    return find_if(d_cumProb.begin(), d_cumProb.end(), 
                [&](double cumProb)
                {
                    return value <= cumProb;
                }
            ) - d_cumProb.begin();
}
