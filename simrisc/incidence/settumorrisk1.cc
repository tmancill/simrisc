//#define XERR
#include "incidence.ih"

void Incidence::setTumorRisk()
{
    auto iter = d_params.cbegin();          // access the incidence parameters

    for (auto &vect: d_tumorRisk)           // resize for all ages
        setTumorRisk(vect, *iter++);
}
