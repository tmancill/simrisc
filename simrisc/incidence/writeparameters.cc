//#define XERR
#include "incidence.ih"

void Incidence::writeParameters(ostream &out) const
{
    out << "    Incidence:\n";

    for (size_t idx = 0; idx != s_carrier.size(); ++idx)
    {
        auto &param = d_params[idx];
        Globals::setPrecision(out, 1) << setw(6) << ' ' << 
           s_carrier[idx] << '\n' << setw(8) << ' ' << "probability:  " << 
            param.prob() << 
            (Globals::isZero(param.prob()) ? 
                    "      (carrier ignored)\n" : "\n");

        Globals::setPrecision(out, 3);

        Globals::setPrecision(out, 2) << 
                setw(8) << ' ' << "lifetimeRisk: " << param.risk() << '\n';
        Globals::setPrecision(out, 1) << 
                setw(8) << ' ' << "mean Age:     " << param.mean() << '\n';
        Globals::setPrecision(out, 2) << 
                setw(8) << ' ' << "std. dev.:    " << 
                                    setw(9) << param.stdDev() << '\n';

        out.put('\n');
    }
}





