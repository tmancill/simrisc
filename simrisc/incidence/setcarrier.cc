//#define XERR
#include "incidence.ih"

void Incidence::setCarrier(size_t idx)
{
    StringVect base{ "Tumor:", "Incidence:", s_carrier[idx], "probability:" };

    Params params;

    bool ok = Parser::proportion(base, params.d_prob);

    base.back() = "stdDev:";
    ok = Parser::positive(base, params.d_stdDev) and ok;

    base.back() = "lifetimeRisk:";

    params.d_vsd[0].setMode(VSD::PROPORTION);
    ok = Parser::one(base, params.d_vsd[0]) and ok;

    base.back() = "meanAge:";

    params.d_vsd[1].setMode(VSD::MEAN);
    ok = Parser::one(base, params.d_vsd[1]) and ok;

    if (ok)
        d_params.push_back(params);
}







