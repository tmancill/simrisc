#define XERR
#include "incidence.ih"

Incidence::Incidence()
{
    for (size_t idx = 0; idx != s_carrier.size(); ++idx)
        setCarrier(idx);

    if (d_params.size() != s_carrier.size())    // some specs are unavailable
        return;

    if (not valid(sumProbs()))                  // probs must sum to 1
    {
        Err::msg(Err::INCIDENCE_SUM_PROB) << endl;
        return;
    }

        // compute the tumor risks per category for END_AGE elements

                                            // room for the age-risks per 
    d_tumorRisk.resize(d_params.size());    // carrier category

    for (auto &vect: d_tumorRisk)           // resize for all ages
        vect.resize(END_AGE);

    setTumorRisk();                         // then set the tumor risks
}
