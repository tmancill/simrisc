//#define XERR
#include "incidence.ih"

// static
void Incidence::writeCarrier(ostream &out, size_t idx, 
                             char type, double value)
{
    out << "         " << type << "_carrier_" << idx << ":\t" <<
            setw(8) << value << '\n';
}
