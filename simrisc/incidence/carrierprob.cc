//#define XERR
#include "incidence.ih"

bool Incidence::carrierProb(StringVect const &base, Params &params)
{
                                                // read the 'probability:' value
    if (not Parser::proportion(base, params.d_prob))
        return false;                           // not found -> done here

    if (Globals::isZero(params.d_prob))         // 0-prob carriers -> all
    {                                           // VSD values are 0
        d_params.push_back(params);
        return false;                   // false because params already pushed
    }        

    return true;
}
