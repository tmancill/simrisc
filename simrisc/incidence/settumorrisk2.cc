//#define XERR
#include "incidence.ih"

// see calcTumorRisk in SimRisc.cpp

void Incidence::setTumorRisk(DoubleVect &ageValues, Params const &params)
{
    size_t age = 0;
    double mean = params.mean().value();

    double factor1 = params.risk().value() / 
                                (params.stdDev() * Globals::s_sqrt2PI);

    for (double &value: ageValues)          // compute the risks per age value
    {
        double factor2 = (age++ - mean) / params.stdDev();
        value = factor1 * exp(-(factor2 * factor2 / 2));
    }
}
