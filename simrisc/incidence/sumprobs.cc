//#define XERR
#include "incidence.ih"

double Incidence::sumProbs()
{
    double sumProb = 0;

    for (Params const &params: d_params)
    {
        sumProb += params.prob();
        d_cumProb.push_back(sumProb);
    }

    return sumProb;
}
