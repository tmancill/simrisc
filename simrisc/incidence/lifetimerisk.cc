#define XERR
#include "incidence.ih"

//static
bool Incidence::lifetimeRisk(StringVect &base, VSD &vsd)
{
    base.back() =  "lifetimeRisk:";

    vsd.setMode(VSD::PROPORTION);
    return Parser::one(base, vsd );
}
