//#define XERR
#include "incidence.ih"

void Incidence::vary()
{
    for (Params &params: d_params)
    {
        if (params.d_prob >= Globals::TOLERANCE)    // if this carrier is used
            params.vary();                          // vary the distribution's
                                                    // value
    }

    setTumorRisk();
}
