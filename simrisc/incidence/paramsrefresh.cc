//#define XERR
#include "incidence.ih"

void Incidence::Params::vary()
{
    for (auto &vsd: d_vsd)
        vsd.vary();
}
