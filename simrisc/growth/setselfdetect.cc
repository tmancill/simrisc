//#define XERR
#include "growth.ih"

void Growth::setSelfDetect()
{
    d_base.back() = "selfDetect:";

    d_mu.setMode(VSD::NON_NEGATIVE);

    Parser::extract(Parser::one(d_base), d_sigma, d_mu);
//    Parser::one(d_base, d_sigma, d_mu);

    if (0 <= d_sigma)
        return;

    Err::negative();
    d_sigma = 0;
}
