//#define XERR
#include "growth.ih"

void Growth::vary()
{
    d_mu.vary();

    for (auto &ageGroupVSD: d_ageGroupVSDs)
        ageGroupVSD.vary();
}
