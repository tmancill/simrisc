//#define XERR
#include "growth.ih"

void Growth::writeParameters(ostream &out) const
{
    out << "  Growth:\n"
           "    diameters:\n";

    out << setw(6) << ' ' << "start:               " << setw(8) << 
                                                            d_start << '\n';
    writeDiameter(out, "self-detect mean:    ", d_mu);
    out << setw(6) << ' ' << "self-detect std.dev.:" << setw(8) << 
                                                            d_sigma << '\n';
    out << "\n"
           "    DoublingTime:\n";

    for (auto const &group: d_ageGroupVSDs)
        out << setw(6) << ' ' << "ageGroup: " << group.ageGroup() << '\n' <<
               setw(10) << ' ' << "mean:     " << group.mean() << '\n' <<
               setw(7) << ' ' << "std.dev: " << 
                                        setw(12) << group.stdDev() << "\n"
               "\n";
}
