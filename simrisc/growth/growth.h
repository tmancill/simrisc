#ifndef INCLUDED_GROWTH_
#define INCLUDED_GROWTH_

//Tumor:
//  Growth:             #   value   spread  dist.  
//      start:              5       
//      selfDetectMu:       2.92     .084   Normal      
//      selfDetectSigma:     .70 
//             
//      DoublingTime:  
//          #                   mean  spread  dist.     stdev
//          ageGroup:  1 - 50:  4.38   .43    Normal     .61 
//          ageGroup: 50 - 70:  5.06   .17    Normal     .26 
//          ageGroup: 70 - * :  5.24   .23    Normal     .45 
                

#include "../agegroupvsd/agegroupvsd.h"

class VSD;

class Growth
{   
    StringVect d_base;

    double d_start;
    VSD d_mu;       
    double d_sigma;

    AgeGroupVSDvect d_ageGroupVSDs;

    public:
        Growth();

        AgeGroupVSD const &ageGroupVSD(uint16_t idx) const; // was: group()

        void vary();

        double start() const;
        VSD const &selfMu() const;
        double selfSigma() const;

        void writeParameters(std::ostream &out) const;

    private:
        void setStart();
        void setSelfDetect();
        void setDoublingTime();

        bool nextRange(AgeGroupVSD const &next) const;


        static void writeDiameter(std::ostream &out, char const *name,
                                  VSD const &vsd);
//         static void writeDT(std::ostream &out, char const *name,
//                             size_t idx, double value);
};

inline AgeGroupVSD const &Growth::ageGroupVSD(uint16_t idx) const
{
    return d_ageGroupVSDs[idx];
}

inline double Growth::start() const
{
    return d_start;
}

inline VSD const &Growth::selfMu() const
{
    return d_mu;
}

inline double Growth::selfSigma() const
{
    return d_sigma;
}

#endif







