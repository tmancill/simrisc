#define XERR
#include "growth.ih"

void Growth::setDoublingTime()
{
    d_base.back() = "DoublingTime:";

    d_base.push_back("ageGroup:");
    auto lines = Parser::any(d_base);
    d_base.pop_back();                          // remove the pushed value
                                                // (for other analyses)

    bool push = true;
    while (true)
    {
        LineInfo const *line = lines.get();

        if (line == 0)
            break;

        AgeGroupVSD ageGroupVSD;

        if (not Parser::extract(*line, ageGroupVSD))
            push = false;

        if (not push)
            continue;

        if (not ageGroupVSD.ageGroup().nextRange(d_ageGroupVSDs))
        {
            push = false;
            continue;
        }

        d_ageGroupVSDs.push_back(ageGroupVSD);
    }
}




