//#define XERR
#include "growth.ih"

// static
void Growth::writeDiameter(ostream &out, char const *label, VSD const &vsd)
{
    out << setw(6) << ' ' << label << setw(8) << vsd << '\n';
}
