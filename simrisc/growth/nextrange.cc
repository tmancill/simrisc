//#define XERR
#include "growth.ih"

bool Growth::nextRange(AgeGroupVSD const &next) const
{
    return d_ageGroupVSDs.empty() 
           or 
           next.beginAge() == d_ageGroupVSDs.back().endAge();
}
