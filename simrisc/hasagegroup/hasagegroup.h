#ifndef INCLUDED_HASAGEGROUP_
#define INCLUDED_HASAGEGROUP_

    // concept used by AgeGroup to ensure that
    // Type has a member ageGroup() returning a const & to an AgeGroup

class AgeGroup;

template <typename Type>
concept HasAgeGroup =
    requires(std::vector<Type> const &object)
    {
           { object.back().ageGroup() } -> std::same_as<AgeGroup const &>;
    };    
        
#endif
