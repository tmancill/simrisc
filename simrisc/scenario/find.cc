//#define XERR
#include "scenario.ih"

// static
int Scenario::find(StringVect const &haystack, string const &needle)
{
    auto iter = std::find(haystack.begin(), haystack.end(), needle);

    if (iter != haystack.end())
        return iter - haystack.begin();

    Err::specification();
    return -1;
}
