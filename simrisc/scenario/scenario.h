#ifndef INCLUDED_SCENARIO_
#define INCLUDED_SCENARIO_


// Scenario:
//   # default values are shown
// 
//                               # use true for variable varying
//   spread:                 false   
//   
//   iterations:             1
//   
//                               # random generator behavior:      
//                               # random, fixed, increasing
//   generator:              random
//   
//                               # initial seed unless using generator: random
//   seed:                   1
// 
//                               # n cases to simulate
//   cases:                  1000

#include "../parser/parser.h"

class Scenario
{
    StringVect d_base;

    bool d_vary;              // true: apply variations to configuration
                                //       parameters

    GeneratorType d_generatorType;

    size_t d_nIterations;
    size_t d_nCases;
    size_t d_seed;

    static StringVect s_generatorType;
    static StringVect s_bool;
    
    public:
        Scenario();

        bool vary() const;        // true: vary to config. params
        size_t nIterations() const;
        size_t seed() const;
        GeneratorType generatorType() const;
        size_t nCases() const;

        void writeParameters(std::ostream &out, size_t iter) const;

    private:
        void setNiterations();              // set n iteratations
        void setNcases();                   // set nCases
        void setSeed();                     // set seed value
        void setGeneratorType();            // set the random generator type
        void setVary();                   // set variableSpred 

        static int find(StringVect const &haystack, 
                        std::string const &needle);
};


inline bool Scenario::vary() const
{
    return d_vary;
}

inline size_t Scenario::nIterations() const
{
    return d_nIterations;
}

inline size_t Scenario::seed() const
{
    return d_seed;
}

inline GeneratorType Scenario::generatorType() const
{
    return d_generatorType;
}

inline size_t Scenario::nCases() const
{
    return d_nCases;
}

#endif






