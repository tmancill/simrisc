#define XERR
#include "scenario.ih"

void Scenario::setGeneratorType()
{
    d_base.back() = "generator:";

    string type;
    if (Parser::one(d_base, type))
    {
        if (int idx = find(s_generatorType, type); idx != -1)
            d_generatorType = static_cast<GeneratorType>(idx);
    }
}

