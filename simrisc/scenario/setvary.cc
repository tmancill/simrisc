#define XERR
#include "scenario.ih"

void Scenario::setVary()
{
    d_base.back() = "spread:";

    string spread;
    if (Parser::one(d_base, spread))
    {
        if (int idx = find(s_bool, spread); idx != -1)
            d_vary = static_cast<bool>(idx);
    }
}









