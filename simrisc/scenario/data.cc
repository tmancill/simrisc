//#define XERR
#include "scenario.ih"

StringVect Scenario::s_generatorType =
{
    "fixed",                            // the SeedType ordering must 
    "increasing",                       // correspond to the order of the
    "random",                           // GeneratorType enum's values 
};



StringVect Scenario::s_bool = 
{
    "false",
    "true"
};
