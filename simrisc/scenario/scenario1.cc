#define XERR
#include "scenario.ih"

Scenario::Scenario()
:
    d_base{ "Scenario:", "" }           // 2nd field set by set-members
{
    setNiterations();
    setNcases();     
    setGeneratorType();   
    setSeed();       
    setVary();
}
