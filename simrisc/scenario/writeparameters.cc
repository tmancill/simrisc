//#define XERR
#include "scenario.ih"

void Scenario::writeParameters(ostream &out, size_t iter) const
{
    out << 
        "Scenario:\n"
        "  iteration:    " << iter << "\n"
        "  generator:    " << s_generatorType[d_generatorType] << "\n"
        "  n iterations: " << d_nIterations << "\n"
        "  n cases:      " << d_nCases << "\n"
        "  seed          " << d_seed << "\n"
        "  spread:       " << s_bool[d_vary] << "\n"
        "\n";
}
