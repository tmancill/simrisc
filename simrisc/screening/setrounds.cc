//#define XERR
#include "screening.ih"

void Screening::setRounds()
{
    d_base.back() = "round:";

    auto lines = Parser::any(d_base);

    bool useRounds = true;
    LineInfo noneLine;

    while (true)
    {
        LineInfo const *line = lines.get(); 
        if (line == 0)
            break;
        
        useRounds = addRound(&noneLine, *line) and useRounds;
    }

    if (d_roundVect.size() and not useRounds)
    {
        Err::reset(noneLine);
        Err::msg(Err::ROUND_NONE) << noneLine.txt << endl;
    }
}
