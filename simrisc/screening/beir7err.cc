//#define XERR
#include "screening.ih"

// static
double Screening::beir7Err(Round const &round, uint16_t biradIdx, double beta, 
                           double eta, Modalities const &modalities)
{
    double risk = 1;
    double factor = beta * pow(round.age() / 60, eta) / 1000;
    
    for (ModBase const *modBase: modalities.use(round.modalityIndices()))
        risk *= (1 + factor * modBase->dose(biradIdx));

    return risk;
}

