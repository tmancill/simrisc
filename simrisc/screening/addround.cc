//#define XERR
#include "screening.ih"

    // false: round: none was specified
    // true: round: age modality/ties was specified

bool Screening::addRound(LineInfo *noneLine, LineInfo const &lineInfo)
{
    Round round(d_modalities.modalityIDs());

    auto in = Parser::extract(lineInfo, round);   // extracts the age

    if (round.none())
    {
        *noneLine = lineInfo;               // save the last round: none line
        return false;
    }

    if (not in)
        return true;

    if (not increasingAge(round))
    {
        Err::msg(Err::ROUND_AGES_DONT_INC) << endl;
        return true;
    }

    string modality;
    while (in >> modality)
    {
        Err::Context context;

        if (uint16_t idx = d_modalities.find(modality); idx == ~0U)
            context = Err::UNDEFINED_MODALITY;
        else
        {
            if (round.add(idx))
                continue;

            context = Err::MODALITY_REPEATED;
        }

        Err::msg(context) << endl;
        return true;
    }

    d_roundVect.push_back(move(round));
    return true;
}
