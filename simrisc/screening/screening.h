#ifndef INCLUDED_SCREENING_
#define INCLUDED_SCREENING_

// Screening:
// 
//     # round: age    space separated modalities. 
//     round:     50  Mammo 
//     round:     52  Mammo 
//     round:     54  Mammo 
//     round:     56  Mammo 
//     round:     58  Mammo 
//     round:     60  Mammo 
//     round:     62  Mammo 
//     round:     64  Mammo 
//     round:     66  Mammo 
//     round:     68  Mammo 
//     round:     70  Mammo 
//     round:     72  Mammo 
//     round:     74  Mammo 

//                              alternatively:
//      round:  none

// 
//     # screening         value
//     attendanceRate:     .8
// 

#include <iosfwd>

#include "../round/round.h"

class Modalities;

class Screening
{
    Modalities const &d_modalities;

    StringVect d_base;

    RoundVect d_roundVect;

    double d_rate;

    public:
        Screening(Modalities const &modalities);

        double rate() const;

        DoubleVect radiationRisk(
                            Modalities const &modalities,
                            Uint16Vect const &indices,
                            double beta, double eta);     // beir7 values

        DoubleVect ages() const;  // vector of ages of the screening rounds

        uint16_t nRounds() const;
        double age(size_t idx) const;   
                                                    // roundIdx is, e.g. 
        Round const &round(size_t roundIdx) const;  // Loop::d_round

        void writeParameters(std::ostream &out) const;

    private:
        void setAttendanceRate();

        void setRounds();
                                            // false: round: none, noneLine
                                            // has the last noneLine
        bool addRound(LineInfo *noneLine, LineInfo const &line);

        bool increasingAge(Round const &round) const;

        static double beir7Err(Round const &round, uint16_t idx, double beta, 
                               double eta, Modalities const &modalities);

};

inline double Screening::rate() const
{
    return d_rate;
}

inline uint16_t Screening::nRounds() const
{
    return d_roundVect.size();
}

inline double Screening::age(size_t idx) const
{
    return d_roundVect[idx].age();
}

inline Round const &Screening::round(size_t idx) const
{
    return d_roundVect[idx];
}


//
//        void beir7dose(double const *dose);                         //  .h
//
//
//
//
//        RoundVect const &rounds() const;
//
//
//        StringSet const &modalities() const;


//     double const *d_beir7dose;      // set from Screening using 
//                                     // beir7dose() 
// 
//     RoundVect d_rounds;
//     StringSet d_modalities;         // specified modalities

//        void set(Distribution *dest,                    // 1
//                 Scenario const &scenario, char const *id);

//                                                        // 2
//        static bool set(Distribution *dest, 
//                         Scenario::const_iterator const &iter);
//

//

// inline StringSet const &Screening::modalities() const
// {
//     return d_modalities;
// }
// 
// inline RoundVect const &Screening::rounds() const
// {
//     return d_rounds;
// }
// 
// inline void Screening::beir7dose(double const *dose)
// {
//     d_beir7dose = dose;
// }

////////////////////////////////////////////////////////////
// inline double Screening::Distribution::value() const
// {
//     return d_value;
// }

//     static char const s_errorID[];
//     static char const s_attendanceID[];
//     static char const s_roundID[];


#endif

