#define XERR
#include "screening.ih"

        // called from loop/initialize.cc

DoubleVect Screening::radiationRisk(
                                Modalities const &modalities,
                                Uint16Vect const &biRadIndices,
                                double beta, double eta)   // beir7 values
{
    DoubleVect radiationRiskVect = DoubleVect(END_AGE, 1);

    for (size_t rndIdx = 0, end = d_roundVect.size(); rndIdx != end; ++rndIdx)
    {
        auto const &round = d_roundVect[rndIdx];    // use round[rndIdx]

                                                    // use the bi-rad index
                                                    // of the round's age
        uint16_t biRadIdx = biRadIndices[rndIdx];   // categroy

        if (Random::instance().uniform() > d_rate)
            continue;

        double error = beir7Err(round, biRadIdx, beta, eta, modalities);

        for (uint16_t age = round.rndAge() + 1; age != END_AGE; ++age)
            radiationRiskVect[age] *= error;
    }

    return radiationRiskVect;
}







