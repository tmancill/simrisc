//#define XERR
#include "screening.ih"

Screening::Screening(Modalities const &modalities)
:
    d_modalities(modalities),
    d_base{ "Screening:", "" }
{
    setAttendanceRate();
    setRounds();
}
