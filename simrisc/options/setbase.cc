//#define XERR
#include "options.ih"

void Options::setBase()
{
    replaceHome(d_base[ANALYSIS]);

    if (d_base[ANALYSIS].back() != '/')
        d_base[ANALYSIS] += '/';
}
