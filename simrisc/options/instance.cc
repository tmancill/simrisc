//#define XERR
#include "options.ih"

// static
Options &Options::instance()
{
    if (s_options == 0)
        s_options = new Options;    // Note: not deleted at end of program

    return *s_options;
}

