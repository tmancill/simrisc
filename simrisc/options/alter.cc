//#define XERR
#include "options.ih"

bool Options::alter(int optChar, std::string const &value)
{
                // if a command-line option was specified, then do not alter
                // options in 'Analysis: specifications 
                //
    if (d_specified[ANALYSIS].find(optChar) != string::npos)   
        return false;                                  

    d_specified[ANALYSIS] += optChar;

    switch (optChar)                    // these options can be altered
    {                                   // they set the [1] parameters
        case 'a':
            deathAge(value);
        break;

        case 'B':
            d_base[ANALYSIS] = value;
        break;

        case 'C':
            d_config[ANALYSIS] = value;
        break;

        case 'D':
            d_dataFile[ANALYSIS] = value; 
        break;

        case 'l':
            lastCase(value);
        break;

        case 'o':
            throw Exception{} << 
                "`one-scenario' cannot be used in scenario specifications";
        break;

        case 'P':
            d_parametersFile[ANALYSIS] = value;
        break;

        case 'R':
            d_roundsFile[ANALYSIS] = value;
        break;

        case 'S':
            d_sensitivityFile[ANALYSIS] = value;
        break;


//        case 'T':
//            setFile(d_cumTotalriskFile = value, 'T');
//        break;

        case 't':
            tumorAge(value);
        break;            

        case 'V':
            d_specified[ANALYSIS] += 'V';
            imsg.on();    
        break;
    }

    return true;
}

