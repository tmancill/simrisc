//#define XERR
#include "options.ih"

void Options::actualize()
{
    d_specificAges = d_specified[STARTUP].find("at") != string::npos;

    replaceHome(d_config[ANALYSIS]);
    setBase();

    replacePlus(d_dataFile[ANALYSIS]);
    replacePlus(d_parametersFile[ANALYSIS]);
    replacePlus(d_roundsFile[ANALYSIS]);
    replacePlus(d_sensitivityFile[ANALYSIS]);

    conflictCheck();                          // prevent conflicting options
}
