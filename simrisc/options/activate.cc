//#define XERR
#include "options.ih"

void Options::activate()
{
    d_specified[ANALYSIS]       = d_specified[STARTUP]; 

    d_config[ANALYSIS]          = d_config[STARTUP];
    d_base[ANALYSIS]            = d_base[STARTUP];

    d_dataFile[ANALYSIS]        = d_dataFile[STARTUP];
    d_parametersFile[ANALYSIS]  = d_parametersFile[STARTUP];
    d_roundsFile[ANALYSIS]      = d_roundsFile[STARTUP];
    d_sensitivityFile[ANALYSIS] = d_sensitivityFile[STARTUP];
}
