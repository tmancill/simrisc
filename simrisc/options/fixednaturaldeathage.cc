//#define XERR
#include "options.ih"

void Options::fixedNaturalDeathAge(double &deathAge) const
{
    if (d_specified[ANALYSIS].find('a') != string::npos)
        deathAge = d_naturalDeathAge;
}
