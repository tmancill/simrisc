//#define XERR
#include "modalities.ih"

Modalities::Modalities(Tumor const &tumor)
:
    d_tumor(tumor)
{
    addMod(new Mammo{ d_tumor });
    addMod(new Tomo);
    addMod(new MRI);
}






