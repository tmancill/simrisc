//#define XERR
#include "modalities.ih"

void Modalities::addMod(ModBase *modBase)
{
    d_modBaseVect.push_back(modBase);
    d_modalityIDs.push_back(modBase->id());
}
