//#define XERR
#include "modalities.ih"

void Modalities::writeRounds(CSVTable &tab, size_t round) const
{
    for (auto modBasePtr: d_modBaseVect)
        tab.more() << (*modBasePtr)[round];
}
