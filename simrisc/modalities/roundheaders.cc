//#define XERR
#include "modalities.ih"

void Modalities::roundHeaders(CSVTable &tab) const
{
    for (auto modBasePtr: d_modBaseVect)                // insert the IDs
        tab.more() << modBasePtr->id();
}
