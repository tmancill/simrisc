#ifndef INCLUDED_MODALITIES_
#define INCLUDED_MODALITIES_

// Modalities:
//
//     Mammo:
//         costs:          64   
// 
//         #      bi-rad:  a       b       c       d             
//         dose:           3       3       3       3  
// 
//         #             ageGroup
//         specificity:  0 - 40:  .961     40 - *: .965
// 
//         #       1       2       3        4      m
//         beta:  -4.38    .49     -1.34    -7.18  .136
// 
//         systematicError:  0.1   
// 
//     Tomo:
//         costs:         64   
// 
//         #      bi-rad:  a       b       c       d             
//         dose:           3       3       3       3  
//         sensitivity:    .87     .84     .73     .65   
// 
//         #             ageGroup
//         specificity:  0 - 40:  .961     40 - *: .965
// 
//     MRI:
//         costs:          280
//         sensitivity:    .94
//         specificity:    .95

#include <iosfwd>
#include <vector>

#include "../typedefs/typedefs.h"
#include "../enums/enums.h"

class ModBase;
class Tumor;

namespace FBB
{
    class CSVTable;
}

class Modalities
{
    Tumor const &d_tumor;               // used by Mammo

                                        
    std::vector<ModBase *> d_modBaseVect;   // defined modalities
    StringVect d_modalityIDs;               // and their IDs

    public:
        Modalities(Tumor const &tumor);
        ~Modalities();

                            // visit all modalities and initialize
                            // their data, see loop/initialize
            // see also Loop::d_nModalityFP: modality specific, to become
            //      a size_t variables in each used modality
        void resetCounters(size_t nRounds);  

                                    // returns ~0U if 'modality' is unknown
        uint16_t find(std::string const &modality) const;

// double specificity(age) const: age may not be negative

            // return a vector of ModBase ptrs matching the names specified
            //  in 'active'
        std::vector<ModBase *> use(Uint16Vect const &active) const;
        StringVect const &modalityIDs() const;


// currently not used 
// double const *hasDose(StringVect const &ids) const;

        void writeParameters(std::ostream &out) const;

        void roundFmt(FBB::CSVTable &tab) const;
        void roundHeaders(FBB::CSVTable &tab) const;
        void writeRounds(FBB::CSVTable &tmp, size_t round) const;

    private:
        void addMod(ModBase *modBase);
};

inline StringVect const &Modalities::modalityIDs() const
{
    return d_modalityIDs;
}

#endif


