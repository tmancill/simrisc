//#define XERR
#include "modalities.ih"

void Modalities::resetCounters(size_t nRounds)
{
    for (auto modBasePtr: d_modBaseVect)
        modBasePtr->resetCounters(nRounds);
}
