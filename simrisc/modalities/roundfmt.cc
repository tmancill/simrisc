//#define XERR
#include "modalities.ih"

void Modalities::roundFmt(CSVTable &tab) const
{
    StringVect ids;
    unsigned totalWidth = 0;

                                                // store modality IDs and
                                                // compute their total table
    for (auto modBasePtr: d_modBaseVect)        // width
    {                                           
        string const &id = modBasePtr->id();
        ids.push_back(id);
        unsigned IDwidth = id.length();
        tab.fmt(tab.size()) << right(IDwidth);   // add this column
        totalWidth += IDwidth;
    }
                                                    // table width of all 
    totalWidth += ids.size() * tab.sep().length();  // Modalities IDs

                                                // to allow for the header:
    if (totalWidth < size("number of"))         // an extra column is needed
    {
        totalWidth = size("number of") - totalWidth - tab.sep().length();
                                                // at least 1 char wide
        tab.fmt(tab.size()) << 
                (static_cast<int>(totalWidth) < 1 ? 1 : totalWidth);
    }
    
    
}
