#ifndef INCLUDED_MRI_
#define INCLUDED_MRI_

#include <iosfwd>

#include "../modbase/modbase.h"

// MRI:
//     costs:          280
//     sensitivity:    .94
//     specificity:    .95


class MRI: public ModBase
{
    double d_sensitivity;
    double d_specificity;

    StringVect d_base;

    public:
        MRI();
        ~MRI() override;

    private:
        void sensitivityHandler();
        void specificityHandler();

        double const *vDose() const override;               // 1
        double vDose(uint16_t idx) const override;          // 2
        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;

};

#endif
