//#define XERR
#include "mri.ih"

void MRI::vInsert(ostream &out) const
{
    Globals::setPrecision(out, 2) <<
            setw(4) << ' ' << "sensitivity:  " << d_sensitivity << '\n' <<
            setw(4) << ' ' << "specificity:  " << d_specificity << '\n';
}
