//#define XERR
#include "mri.ih"

void MRI::specificityHandler()
{
    d_base.back() = "specificity:";
    Parser::proportion(d_base, d_specificity);
}
