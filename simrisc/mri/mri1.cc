//#define XERR
#include "mri.ih"

// configfile lines: 
//      MRI cost:           280
//      MRI sensitivity:    0.94
//      MRI specificity:    0.95
                    

MRI::MRI()
:
    ModBase("MRI"),
    d_base{ "Modalities:", "MRI:", "" }
{
    costHandler(d_base);
    specificityHandler();
    sensitivityHandler();
}



