#define XERR
#include "random.ih"

    //  when use() is called, reinit -> reset has defined the
    //  vary-generators and vary-engines

    
void Random::use(DistType distType)
{
    switch (distType)
    {
        case NORMAL_VARY:
            if (not d_normalVary)
                d_normalVary.reset(new normal_distribution<>{});
        break;

        case UNIFORM_VARY:
            if (not d_uniformVary)
                d_uniformVary.reset(new uniform_real_distribution<>{});
        break;

        case LOGNORMAL_VARY:
            if (not d_logNormalVary)
                d_logNormalVary.reset(new lognormal_distribution<>{});
        break;

        default:
        break;
    }
}
