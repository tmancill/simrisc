//#define XERR
#include "random.ih"

double Random::logNormal(double mean, double stdDev)
{
    d_logNormal.param(LogNormalParams{ mean, stdDev });

    return d_logNormal(d_engine[LOGNORMAL]);
}
