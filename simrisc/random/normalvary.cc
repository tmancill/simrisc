//#define XERR
#include "random.ih"

double Random::normalVary()
{
    return (*d_normalVary)(d_engine[NORMAL_VARY]);
}

////////////////////////////////////////////
// ?? 
// d_normal is the std. normal distribution.  To obtain a value from the
// std. normal distribution return d_normal(d_engine)
//    double x1 = uniform();
//    double x2 = uniform();
//    double y = sqrt( -2 * log(x1) );
//
//    return y * sin( 2 * M_PI * x2 );
