#define XERR
#include "random.ih"

void Random::reset()
{
    size_t seed = d_seed;

    for (size_t idx = 0; idx != N_STD_DISTRIBUTIONS; ++idx)
        d_engine[idx] = mt19937{seed <<= 1};

    d_uniform.reset();
    d_uniformCase.reset();
    d_logNormal.reset();

    if (d_vary)
    {
        seed <<= 1;
        if (d_normalVary)
        {
            d_engine[NORMAL_VARY] = mt19937{seed};
            d_normalVary->reset();
        }

        seed <<= 1;
        if (d_uniformVary)
        {
            d_engine[UNIFORM_VARY] = mt19937{seed};
            d_uniformVary->reset();
        }

        seed <<= 1;
        if (d_logNormalVary)
        {
            d_engine[LOGNORMAL_VARY] = mt19937{seed};
            d_logNormalVary->reset();
        }
    }
}

//    d_chi2->reset();
//    d_binomial->reset();
//    d_exponential.reset(exponential_distribution<>);
