//#define XERR
#include "random.ih"

double Random::binomial(double proportion)
{
    d_binomial->param(BinomialParams{ N_TRIALS, proportion });

    return (*d_binomial)(d_engine[BINOMIAL]) / static_cast<double>(N_TRIALS);
}

