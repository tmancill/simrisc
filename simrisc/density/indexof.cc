//#define XERR
#include "density.ih"

uint16_t Density::indexOf(double prob) const
{
    return find_if(d_cumBirads, d_cumBirads + N_BIRADS, // visit
                [&](double cumProb)
                {
                    return prob <= cumProb;
                }
            ) - d_cumBirads;
}
