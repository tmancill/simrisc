//#define XERR
#include "density.ih"

std::ostream &Density::insert(ostream &out) const
{
    out << "ageGroup: " << d_ageGroup << ", bi-rad: ";

    for (size_t idx = 0; idx != N_BIRADS; ++idx)
        out << static_cast<char>('a' + idx) << ": " << 
                setw(4) << d_birad[idx] << ", ";

    return out;
}
