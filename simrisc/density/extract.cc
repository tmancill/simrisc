//#define XERR

#include "density.ih"

//     ageGroup:    0  - 40: ->   
//                              .05    .30    .48    .17

istream &Density::extract(istream &in)
{
    in >> d_ageGroup;

    double cumProb = 0;

    for (size_t idx = 0; idx != N_BIRADS; ++idx)
    {
        double probability;
        in >> probability;

        d_birad[idx] = probability;

        cumProb += probability;
        d_cumBirads[idx] = cumProb;
    }

    return in;
}
