#ifndef INCLUDED_TYPEDEFS_
#define INCLUDED_TYPEDEFS_

#include <vector>
#include <cstdint>
#include <string>
#include <unordered_set>

#include "../enums/enums.h"

//struct
struct LineInfo
{
    ParamsSrc   src;
    uint16_t    lineNr;
    std::string txt;
    std::string tail;
};        
//=
//types
typedef std::unordered_set<std::string> StringSet;
typedef std::vector<LineInfo>           LineInfoVect;
typedef std::vector<double>             DoubleVect;
typedef std::vector<DoubleVect>         DoubleVect2;
typedef std::vector<size_t>             SizeVect;
typedef std::vector<std::string>        StringVect;
typedef std::vector<uint16_t>           Uint16Vect;
//=

#endif
