//#define XERR
#include "loop.ih"

//      d_pool->falsePositives(d_round * (modality + 1)) >
//        d_modalities[XRAY][modality == MAMMOGRAPHY ? MAMMO : TOMO]
//                    [screeningAge >= 40].value()


void Loop::maybeFalsePositive(ModBase *modBase, double screeningAge)
{
    if (Random::instance().uniform() > modBase->specificity(screeningAge))
    {
        modBase->falsePositive();
        ++d_nRoundFP[d_round];
        addCost(d_costs.biopsy(screeningAge)); 
    }
}
