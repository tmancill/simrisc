//#define XERR
#include "loop.ih"

void Loop::writeSensitivity(CSVTable &tab, size_t iter) const
{
    if (tab.stream())
        tab <<  iter << static_cast<size_t>(round(d_sumDeathAge)) << 
                        static_cast<size_t>(round(d_totalCost));
}







