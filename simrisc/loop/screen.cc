//#define XERR
#include "loop.ih"

// perform a screening

void Loop::screen(double screeningAge)
{
    // visit all modalities: for (ModBase *...) or comparable

                                            // org/screen: 4, screen.txt: 1
    for (ModBase *modBase: d_modalities.use(
                                d_screening.round(d_round).modalityIndices())
    )
    { 
        if (not use(modBase))
            continue;

        addCost(d_costs.screening(screeningAge, modBase->cost()));

        if (d_tumor) 
            d_tumor.characteristics(screeningAge); 

                        // test2 (see below)
        if (d_tumor and screeningAge >= d_tumor.detectableAge()) 
            maybeDetect(modBase, screeningAge);
        else 
            maybeFalsePositive(modBase, screeningAge);
    }
}

// tumor   test2   maybedetect maybefalsepos
//   1       1         1           0
//   1       0         0           1
//   0       1         0           1
//   0       0         0           1
// 
// -> calling maybeFalsePositive may not be called when just 'not d_tumor'
//    is true.


//xerr("");
//for (auto const &id: d_screening.round(d_round).modalityIDs())
//cerr<<"     id: " << id <<'\n';


//xerr("use " << modBase->id());

//xerr("cpt costs");

