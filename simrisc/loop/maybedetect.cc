#define XERR
#include "loop.ih"

void Loop::maybeDetect(ModBase *modBase, double screeningAge)
{
    // see also: Marcel: Tue, 9 Jun 2020 13:01:00 +0000
    //           Marcel: Thu, 11 Jun 2020 10:35:44 +0000 (MRI, Mammo, Tomo are
    //                   all used

    // the check for tumor.diameter() >= 1 is omitted, since
    // all tumors have at least 1 mm diameter 
    // (see Marcel: Tue, 30 Jun 2020 10:46:05 +0000)

    // modified using Jing Wang's email (Tue, 20 Oct 2020 16:04:42 +0000)

    if (Random::instance().uniform() > sensitivity(modBase))
    {
        ++d_nRoundFN[d_round];
        return;
    }
                // Found a tumor during the screening -> no need to call
                // diameterCheck() 
    d_tumor.detected();
    ++d_nDetections[d_round];

    addCost(d_costs.treatment(screeningAge, d_tumor.diameter()));  
    d_tumor.setDeathAge(screeningAge);

    d_roundDetected = d_round;

        // if this condition is true then a tumor was found, but the case
        // dies naturally (i.e., not caused by the cancer).
        // Otherwise death is caused by the cancer
    if (d_naturalDeathAge < d_tumor.deathAge()) 
        setStatus(LEFT_DURING, d_naturalDeathAge);
    else
        setStatus(TUMOR_DURING, d_tumor.deathAge());
}

/////////////////////////////////////////////////////////////////////////
//     if (betaFunction(modBase->nr()))
//     {
//                     // Found a tumor during the screening -> no need to call
//                     // diameterCheck() 
//         d_tumor.detected();
//         ++d_nDetections[d_round];
// 
//         addCost(d_costs.treatment(screeningAge, d_tumor.diameter()));  
//         d_tumor.setDeathAge(screeningAge);
// 
//         d_roundDetected = d_round;
// 
//             // if this condition is true then a tumor was found, but the case
//             // dies naturally (i.e., not caused by the cancer).
//             // Otherwise death is caused by the cancer
//         if (d_naturalDeathAge < d_tumor.deathAge()) 
//             setStatus(LEFT_DURING, d_naturalDeathAge);
//         else
//             setStatus(TUMOR_DURING, d_tumor.deathAge());
//     }
// 
//     if (d_random.uniform() > sensitivity(modBase))
//         ++d_nRoundFN[d_round];
// 
