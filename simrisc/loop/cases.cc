//#define XERR
#include "loop.ih"

        // if last case (--nCases) was specified, use that number of cases
        // if death-age (--death-age) was specified simulate one case having
        // the specified death age. Otherwise use nWomen cases

size_t Loop::cases() const
{
    auto const &options = Options::instance();

    return 
        options.lastCase() != 0 ? options.lastCase()    :
        options.specificAges()  ? 1                     :
                                  d_scenario.nCases();
}
