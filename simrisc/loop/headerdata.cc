//#define XERR
#include "loop.ih"


//  woman, death cause, death age, tumor present, tumor detected, tumor is
// interval, tumor diameter, tumor self-detect age, tumor death age, tumor
// doubling time, tumor onset age, tumor preclinical period, natural death age,
// death status, cdsts, self detected, round detected,  

CSVTable Loop::headerData(size_t iter) const
{
    CSVTable tab{ outStream(Options::instance().dataFile(), iter), "  " };

    if (not tab.stream())
        return tab;

    labels(tab.stream());

    tab.fmt() <<  right(7) <<                                   //  1     
                  "Natural" << right("100.12", 2) <<            //  2 -  3
                  "natural" << "status" <<                      //  4 -  5
                  "present" << "detected" << "interval" <<      //  6 -  8
                  "diameter" << "doubling" << "preclinical" <<  //  9 - 11

                                           //  death:     
                  "onset" << "self-detect" << "100.00" <<       // 12 - 14
                    "costs" << "self" << "round";               // 15 - 17

    tab << hline();
                                                                // 6 - 14 + hline
    tab.row(5) << join(9, FMT::CENTER) << "tumor";
    tab.row(5) << hline(9);

    tab.row(1) <<                                               //  1
            join(4, FMT::CENTER) << "death" <<                  //  2 -  5
            join(6, FMT::RIGHT) << ' ' <<                       //  4 -  9
            ' ' <<                                              // 10
            join(2, FMT::CENTER) << "age" <<                    // 11 - 12
            ' ' <<                                              // 15
            join(2, FMT::CENTER) << "detected";                 // 16 - 17


    tab.row(1) <<                                               //  1
            hline(4) <<                                         //  2 -  3
            join(4, FMT::RIGHT) << ' ' <<                       //  4 -  7
            "doubling" << "preclinical" << ' ' <<               //  8 - 10
            hline(2) <<                                         // 11 - 12
            ' ' <<                                              // 13
            hline(2);                                           // 14 - 15

    tab << "case" <<                                            //  1     
            "cause" << "age" <<                                 //  2 -  3
            "natural" << "status" <<              // 13 - 15
            "present" << "detected" << "interval" <<            //  4 -  6
            "diameter" << "days" << "period" <<                 //  7 -  9
            "onset" << "self-detect" << "death" <<              // 10 - 12
            "costs" <<             // 13 - 15
            "self" << "round";                                  // 16 - 17

    tab << hline();

    tab.sep(", ");

    return tab;
}

//         out <<
// 1           " woman, "
// 2           "death cause, "
// 3           "death age, "
// 4           "tumor present, "
// 5           "tumor detected, "
// 6           "tumor is interval, "
// 7           "tumor diameter, "

// 8  11       "tumor self-detect age, "
// 9  12       "tumor death age, "
// 10 8        "tumor doubling time, "
// 11 10       "tumor onset age, "
// 12 9        "tumor preclinical period, "

// 13          "natural death age, "
// 14          "death status, "
// 15          "costs, "
// 16          "self detected, "
// 17          "round detected, "
//             "\n";
//     }








