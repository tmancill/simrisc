//#define XERR
#include "loop.ih"

void Loop::postScreen()
{
xerr("status: " << d_status);

    if (d_status != PRESENT)          // the woman has died: no further actions
        return;

xerr("treatment cost entry: " << d_caseCost);

                                    // there is a tumor, detected before the
                                    // woman's natural death
    if (d_tumor and d_tumor.age() < d_naturalDeathAge) 
        treatmentDeath();           // this results in a tumor caused death.
                                    // (the contition check in the original
                                    //  sources is superfluous)

    else                            // or a naturally caused death
    {
        setStatus(LEFT_POST, d_naturalDeathAge);
        
                                    // although the woman has died, still
        if (d_tumor)                // determine the tumor's characteristics
            d_tumor.characteristics(d_deathAge);
    }
}



