#define XERR
#include "loop.ih"

// -----------------------------------
// iteration  women years  total costs
// -----------------------------------
//         0,         795,       13478
// -----------------------------------


CSVTable Loop::headerSensitivity() const
{
    CSVTable tab{ outStream(Options::instance().sensitivityFile(), 0), "  " };

    if (not tab.stream())
        return tab;

    labels(tab.stream());

    tab.fmt("iteration, women years, total costs");

    tab << hline();
    tab("iteration, women years, total costs");
    tab << hline();

    return tab;
}





