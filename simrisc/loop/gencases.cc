#define XERR
#include "loop.ih"

// -> ORG/womanloop.cpp

void Loop::genCases(size_t iter, size_t nCases)
{
    CSVTable dataTable = headerData(iter);

    auto const &options = Options::instance();

    bool showAll = not options.specificAges() and options.lastCase() == 0;

                                        // perform 'nCases' simulations
    for (size_t caseIdx = 0; caseIdx != nCases; ++caseIdx)
    {
        caseInit();
        preScreen();        // no action unless screening rounds are specified
        screening();        // same
        postScreen();

        d_sumDeathAge += d_deathAge;
        d_totalCost += d_caseCost;

        if (dataTable.stream() and (showAll or caseIdx + 1 == nCases))
            writeData(dataTable, caseIdx);
    }

    dataTable << hline();

}

// In the original womanloop function a test is performed whether the
// woman has died or not. This test is superfluous because if the woman enters
// postScreen with status PRESENT then in postScreen the status either changes
// to LEFT_POST or TUMOR_POST.





