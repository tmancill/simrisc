//#define XERR
#include "loop.ih"

// static unsigned idx = 0;

double Loop::naturalDeathAge()
{
//    double rnd;
    double ret = findAge(s_cumDeathProb, 
//                         rnd = 
                            Random::instance().uniformCase());

xerr(idx++ << ": nat.death: " << ret << ", rnd: " << rnd);

    return ret >= MAX_AGE ? MAX_AGE : ret;
}
