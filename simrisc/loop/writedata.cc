#define XERR
#include "loop.ih"

void Loop::writeData(CSVTable &tab, size_t idx) const
{
    tab.more() << idx <<                                        //  1     
           (d_status >= TUMOR_PRE ? "Tumor" : "Natural") <<     //  2 
           d_deathAge << d_naturalDeathAge << d_status;         //  3 - 5

    d_tumor.writeData(tab);                                     //  6 - 14

    tab << static_cast<size_t>(round(d_caseCost)) <<            // 15
           d_selfDetected << d_roundDetected;                   // 16 - 17
}




