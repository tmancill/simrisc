//#define XERR
#include "loop.ih"

void Loop::intervalCancer()
{
    // a self-detected tumor before the next screening: an interval cancer.

    d_tumor.intervalCancer();
    ++d_nIntervals[d_round];

    d_selfDetected = true;
    
    d_tumor.characteristics(); 
    
        // treat the tumor and determine the death-age of the women
        // since the tumor is present, no need to call diameterCheck()
    addCost(d_costs.treatment(d_tumor.age(), d_tumor.diameter()));

    d_tumor.setDeathAge();
    
    if (d_naturalDeathAge < d_tumor.deathAge())     // natural death
         setStatus(LEFT_DURING, d_naturalDeathAge);
    else                                            // death by tumor
        setStatus(TUMOR_DURING, d_tumor.deathAge());
}




