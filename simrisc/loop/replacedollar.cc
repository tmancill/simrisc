//#define XERR
#include "loop.ih"

// static
string Loop::replaceDollar(string const &settingsFile, size_t idx)
{
    string ret{ settingsFile };
    string nr{ to_string(idx) };

    idx = 0;
    while (true)
    {
        idx = ret.find('$', idx);               // find the next  $

        if (idx == string::npos)                // no more
            return ret;

        ret.replace(idx, 1, nr);                // replace the $ by nr
    }        
}
