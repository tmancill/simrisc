//#define XERR
#include "loop.ih"

void Loop::writeRounds(CSVTable &tab) const
{
    if (not tab.stream() or d_nRounds == 0)
        return;

    for (size_t rnd = 0; rnd != d_nRounds; ++rnd)
    {
        tab.more() << rnd << 
                      d_nRoundFP[rnd] << d_nRoundFN[rnd] <<
                      d_nDetections[rnd] << d_nIntervals[rnd] << 
                      d_roundCost[rnd];

        d_modalities.writeRounds(tab, rnd);
        tab.row();
    }

    tab << hline();
}
