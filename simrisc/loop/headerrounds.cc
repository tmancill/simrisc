//#define XERR
#include "loop.ih"

CSVTable Loop::headerRounds(size_t iteration) const
{
   CSVTable tab{ outStream(
        d_nRounds == 0 ? "" : Options::instance().roundsFile(), iteration),
                 "  " };

//          false        number of                   number of
//         ---------  ----------------  screening  --------------
//  round  pos. neg.  tumors  interval   costs     Mammo Tomo MRI
//  -------------------------------------------------------------

    if (not tab.stream() or d_nRounds == 0)
        return tab;

    labels(tab.stream());        

    tab.fmt() << "round" <<                // 1: round
                "pos." << "neg." <<         // 2-3
                "tumors" << "interval" <<   // 4-5
                "screening";                // 6

//  E.g., "Mammo" << "Tomo" << "MRI"; // 7-9
    d_modalities.roundFmt(tab);

    tab << hline();
    tab.row(1) << join(2, FMT::CENTER) << "false"       <<    //  2-3
                  join(2, FMT::CENTER) << "number of"   <<    //  4-5
                  ' '                                   <<    //  6
                  join(tab.size() - 6, FMT::CENTER) << "number of";
    tab.row(1) << hline(2) << hline(2) << "screening" << 
                  hline(tab.size() - 6);

    tab.more() << "round" << "pos." << "neg." <<        // 1-3
                "tumors" << "interval" <<               // 4-5
                "costs";                                // 6

    d_modalities.roundHeaders(tab);
    tab.row();

    tab << hline();

    tab.sep(", ");

    return tab;
}
    //  "# Women with a tumor present\t"  // not computed in org. prog.
    //  (see rounds2.cc)

