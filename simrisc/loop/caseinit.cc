#define XERR
#include "loop.ih"

void Loop::caseInit()
{
    d_caseCost = 0;                         // org: womenCosts
    d_deathAge = 0;                         // org: tumorDeathAge
    d_roundDetected = 0;                    // org: roundDetected
    d_selfDetected = false;                 // org: selfDetected

                                // Breast bi-rad indices to use for the
                                // ages of the screening rounds

    d_biRadIndices = d_densities.biRadIndices(d_screening.ages());

    d_tumorInfo.cumTotalRisk(
                    d_screening.radiationRisk(
                            d_modalities,
                            d_biRadIndices,
                            d_tumorInfo.beir7().beta(), 
                            d_tumorInfo.beir7().eta()
                    )
                );

    Options::instance().fixedNaturalDeathAge(
                                    d_naturalDeathAge = naturalDeathAge());
    d_status = PRESENT;

    d_tumor.reset();
}



