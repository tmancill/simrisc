//#define XERR
#include "loop.ih"

void Loop::characteristics(Status natural, Status tumor)
{
                                // the tumor was self-detected: determine
    d_tumor.characteristics();  // its characteristics

    d_selfDetected = true;

        // treat the tumor: add the treatment cost and compute the age of
        // tumor-caused death. There is a tumor: no need to call
        // diameterCheck()
    d_caseCost += d_costs.treatment(d_tumor.age(), d_tumor.diameter());
    d_tumor.setDeathAge();

    if (d_naturalDeathAge < d_tumor.deathAge())     // naturally caused death
        setStatus(natural, d_naturalDeathAge);
    else                                            // or tumor caused death
        setStatus(tumor, d_tumor.deathAge());
}

// double cost = d_costs.treatment(d_tumor.age(), d_tumor.diameter()); 
// xerr("costs: " << cost << ", age: " << d_tumor.age() << ", dia: " <<[bsl]
// d_tumor.diameter());
// d_caseCost += cost;

