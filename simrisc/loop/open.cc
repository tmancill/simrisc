//#define XERR
#include "loop.ih"

// static
ofstream Loop::open(string const &fname)
{
                                        // already saw fname 
    if (s_availableFiles.find(fname) != s_availableFiles.end())
    {
        ofstream out{ Exception::factory<ofstream>(fname, 
                                            ios::in | ios::out | ios::ate) }; 
        out << setfill('-') << setw(40) << '-' << setfill(' ') << "\n\n";
        return out;
    }

    s_availableFiles.insert(fname);
    return Exception::factory<ofstream>(fname);
}
