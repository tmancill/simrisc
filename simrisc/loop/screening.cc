//#define XERR
#include "loop.ih"

void Loop::screening()
{
    for (d_round = 0; d_status == PRESENT and d_round != d_nRounds; ++d_round)
    {
        double screeningAge = d_screening.age(d_round);

        if (leaving(screeningAge))
            return;

xerr("round: " << d_round);

        screen(screeningAge);
    }
}
