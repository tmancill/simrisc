//#define XERR
#include "loop.ih"

bool Loop::leaving(double screeningAge)
{
    xerr("natural: " << d_naturalDeathAge << ", screen age: " << screeningAge);

    if (                                        // see below
            screeningAge  <= d_naturalDeathAge
            and
            (not d_tumor or screeningAge <= d_tumor.age())
    )
        return false;                       // no natural Death

        // determine whether the woman died between the previous and current
        // screening rounds
    if (d_tumor and d_tumor.age() <= d_naturalDeathAge) 
        intervalCancer();
    else
    {
        setStatus(LEFT_DURING, d_naturalDeathAge);
        
        if (d_tumor)    // a tumor was developed
            d_tumor.characteristics(d_deathAge);    // d_deathAge now equal
                                                    // to d_naturalDeathAge
    }
        
    return true;
}

//  negating the original condition to return false if the woman hasn't died:
//  return false if death didn't occur:
//        not (
//            d_naturalDeathAge < screeningAge 
//            or
//            (d_tumor and d_tumor.age() < screeningAge)
//        )

//    if (d_tumor)
//    xerr("self detect age: " << d_tumor.age());


