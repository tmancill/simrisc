//#define XERR
#include "loop.ih"

// Woman dies because of natural cause before screening and
// does not have tumor or dies before self-detecting the tumor

void Loop::preTumorDeath()
{
    setStatus(LEFT_PRE, d_naturalDeathAge);

    if (d_tumor)        // would have developed a tumor. Compute the tumor's
        d_tumor.characteristics(d_deathAge);    // characteristics
}
