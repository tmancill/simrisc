//#define XERR
#include "loop.ih"

// static
ofstream Loop::outStream(string const &fname, size_t idx) 
{
    ofstream ret;

    if (fname.empty())
        ret.setstate(ios::failbit);
    else
        ret = open(replaceDollar(fname, idx));

    return ret;
}
