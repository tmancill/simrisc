//#define XERR
#include "loop.ih"

void Loop::preScreen()
{
    // see prescreen.txt

    if (d_nRounds == 0)         // no scenario rounds, no pre-screening
        return;
                                // using the complement of the prescreen 
    if (                        // in the original code
        d_naturalDeathAge >= d_screening.age(0)
        and 
        (not d_tumor or d_tumor.age() >= d_screening.age(0))
    )
        return;

        // pre-screening is performed:

    if (not d_tumor or (d_tumor and d_naturalDeathAge < d_tumor.age()))
        preTumorDeath();    // natural death: no tumor or not self-detected
    else
        selfDetected(); // self detected the tumor before the screening
}







