//#define XERR
#include "loop.ih"

void Loop::addCost(double cost)
{
xerr("costs: " << cost);

    d_caseCost += cost;

    d_roundCost[d_round] += round(cost);        // round(): cmath
}
