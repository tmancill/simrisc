#define XERR
#include "loop.ih"

Loop::Loop(StringVect const &labels)
:
    d_labels(labels),
    d_tumor(d_tumorInfo),
    d_modalities(d_tumor),
    d_screening(d_modalities),
    d_nRounds(d_screening.nRounds()),

    d_timestamp(DateTime{ DateTime::LOCALTIME }.rfc2822())
{}
