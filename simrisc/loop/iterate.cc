#define XERR
#include "loop.ih"

    // -> ORG/niterationsloop.cpp

void Loop::iterate()
{
    CSVTable sensTable = headerSensitivity();   // table to collect
                                                // sensitivity values

    Random::instance().setSeed(d_scenario.seed());

    for (size_t idx = 0, end = d_scenario.nIterations(); idx != end; ++idx)
    {
        size_t nCases = cases();

        Random::instance().reinit(nCases, d_scenario.vary(), 
                                          d_scenario.generatorType());  

        if (d_scenario.vary())      // maybe vary the parameters
            d_tumorInfo.vary();

        writeParameters(idx);       // write the actual parameter values,

        CSVTable rounds = headerRounds(idx);

        resetCounters();

        genCases(idx, nCases);

        writeRounds(rounds);        // -> ORG/loopendout.cpp

        writeSensitivity(sensTable, idx);
    }

    if (sensTable.stream())
        sensTable << hline();
}





