//#define XERR
#include "densities.ih"

void Densities::writeParameters(std::ostream &out) const
{
    out << 
        "BreastDensities:\n";

    Globals::setPrecision(out, 2);

    for (Density const &density: d_densities)
        out << "  " << density << '\n';

    out.put('\n');
}
