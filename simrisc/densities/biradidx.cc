//#define XERR
#include "densities.ih"

uint16_t Densities::biradIdx(double age, double prob) const
{
    for (Density const &density: d_densities)   
    {
        if (density.contains(age))
            return density.indexOf(prob);   // find the prob. in the sequence
    }                                       // of cumulative density probs

    throw Exception{} << "runtime-error: no age category for age " << age;
}
