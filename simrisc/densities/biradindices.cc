//#define XERR
#include "densities.ih"

Uint16Vect Densities::biRadIndices(DoubleVect const &ages) const
{
    Uint16Vect ret;

    double prob = Random::instance().uniform();

    for (double age: ages)
        ret.push_back(biradIdx(age, prob));

    return ret;
}
