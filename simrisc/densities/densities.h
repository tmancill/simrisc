#ifndef INCLUDED_DENSITIES_
#define INCLUDED_DENSITIES_

//  example config spec.:

// BreastDensities:
// 
//     #                bi-rad:  a      b      c      d
//     agegroup:    0  - 40:    .05    .30    .48    .17
//     agegroup:    40 - 50:    .06    .34    .47    .13
//     agegroup:    50 - 60:    .08    .50    .37    .05
//     agegroup:    60 - 70:    .15    .53    .29    .03
//     agegroup:    70 - * :    .18    .54    .26    .02

#include <iosfwd>

#include "../density/density.h"
#include "../typedefs/typedefs.h"

class Densities
{
    StringVect  d_base;
    DensityVect d_densities;

    public:
        Densities();
                                        // randomly determined bi-rad indices
                                        // for screening round ages
        Uint16Vect biRadIndices(DoubleVect const &ages) const;

        void writeParameters(std::ostream &out) const;

    private:
        void add(bool *checkRange, LineInfo const &ageGroup);

        bool nextRange(Density const &next) const;

                                                // birad idx for age, given 
                                                // its probability
        uint16_t biradIdx(double age, double prob) const;
};
        
#endif



