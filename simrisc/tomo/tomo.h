#ifndef INCLUDED_TOMO_
#define INCLUDED_TOMO_

#include "../enums/enums.h"
#include "../modbase/modbase.h"
#include "../specificity/specificity.h"


// Tomo:
//     costs:         64   
// 
//     #      bi-rad:  a       b       c       d             
//     dose:           3       3       3       3  
//     sensitivity:    .87     .84     .73     .65   
//     #             ageGroup
//     specificity:  0 - 40:  .961     40 - *: .965


class Tomo: public ModBase
{
    StringVect d_base;

    double d_dose[N_BIRADS];
    double d_sens[N_BIRADS];


    SpecificityVect d_specificity;

    public:
        Tomo();
        ~Tomo() override;

    private:
        void doseHandler();
        void sensitivityHandler();

        double const *vDose() const override;               // 1
        double vDose(uint16_t) const override;              // 2
        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;

};

#endif




