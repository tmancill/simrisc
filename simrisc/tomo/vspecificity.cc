//#define XERR
#include "tomo.ih"

// E.g., age groups:
//             0-40 
//             40-100
// then by searching from the last to the first: find the agegroup 
// where the begin-age is < specified age. 
// Examples: age 50: the last group,
//           age 40: the first group
// 'age' may not be negative
    
double Tomo::vSpecificity(double age) const
{
    return find_if(d_specificity.rbegin(), d_specificity.rend(), 
                    [&](Specificity const &spec)
                    {
                        return spec.beginAge() < age;
                    }
            )->value();
}
