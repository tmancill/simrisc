//#define XERR
#include "tomo.ih"

// Tomo cost: 64   
// 
// #    bi-rad cat:    a       b       c       d             
// Tomo dose:          3       3       3       3  
// Tomo sensitivity:   0.87    0.84    0.73    0.65   
// 
// #                   agegroup
// Tomo specificity:   0 - 40:   0.961     40 - *: 0.965

Tomo::Tomo()
:
    ModBase("Tomo"),
    d_base{ "Modalities:", "Tomo:", "" }
{
    costHandler(d_base);                // ModBase handles the costs spec.
    doseHandler();
    sensitivityHandler();
    specificityHandler(d_specificity, d_base);
}



