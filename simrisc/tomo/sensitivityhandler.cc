//#define XERR
#include "tomo.ih"

void Tomo::sensitivityHandler()
{
    d_base.back() = "sensitivity:";
    doubleHandler(d_sens, d_base, &Globals::proportions);
}
