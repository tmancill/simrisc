//#define XERR
#include "tomo.ih"

void Tomo::vInsert(ostream &out) const
{
    insertBirads(Globals::setPrecision(out, 1), "dose:        ", d_dose);
    insertBirads(Globals::setPrecision(out, 2), "sensitivity: ", d_sens);
    insertSpecificity(out, 8, d_specificity);
}
