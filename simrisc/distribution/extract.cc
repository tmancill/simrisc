#define XERR
#include "distribution.ih"

    // the Distribution is extracted if d_value is present. 
    // if present it must be >= 0
istream &Distribution::extract(istream &in)
{
    if (not (in >> d_value))            // nothing there to extract
    {
        d_type = NORMAL_VARY;
        prepareVary();
        in.clear();
        return in;
    }

    if (d_value < 0)
    {
        Err::msgTxt(Err::NEGATIVE);
        in.fail();
        return in;
    }
        
        // value was extracted, so dist must be present.

    string dist;

    if (in >> dist and (d_type = find(dist)) != N_DISTRIBUTIONS)
    {
        prepareVary();
        return in;
    }

    Err::msgTxt(Err::UNDEFINED_DIST);
    in.fail();

    return in;
}




