#define XERR
#include "distribution.ih"

void Distribution::setVary(VaryType varyType)
{
    d_vary = varyType == VARY_POS  ? &Distribution::varyPos  :
             varyType == VARY_PROB ? &Distribution::varyProb :
                                     &Distribution::varyOK;
}
