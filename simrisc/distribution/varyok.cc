#define XERR
#include "distribution.ih"

// in the original code refreshing is handled by reloading the complete config
// file. So the argument passed to this member should be the initial value
// and not the updated value.

double Distribution::varyOK(double orgValue) const
{
    Random &random = Random::instance();

    switch (d_type)
    {
        case NORMAL_VARY:
        return orgValue + random.normalVary() * d_value;

        case UNIFORM_VARY:
        return orgValue + d_value * (random.uniformVary() - .5);

        case LOGNORMAL_VARY:
        return random.logNormalVary(orgValue, d_value);

        default:
        return orgValue;
    }
}

//        case EXPONENTIAL:
//        return orgValue + random.exponential(orgValue);
        





