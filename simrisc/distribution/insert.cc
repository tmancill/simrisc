//#define XERR
#include "distribution.ih"

std::ostream &Distribution::insert(std::ostream &out) const
{
    if (d_type != N_DISTRIBUTIONS)
        out << d_value << " (" << s_name[d_type] << ')';
    else
        out << setw(12) << ' ';

    return out;
}
