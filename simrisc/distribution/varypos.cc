#define XERR
#include "distribution.ih"

double Distribution::varyPos(double orgValue) const
{
    for (unsigned count = 0; count != MAX_VARY_TRIES; ++count)
    {                                       
                                    // try to obtain a valid spreaded SD
        if (double ret = varyOK(orgValue); ret >= 0)
            return ret;
    }

    throw Exception{} << 
        "failed to obtain non-negative spreaded value for " <<
        orgValue << " (spread = " << d_value << 
                    ", distribution: " << name(d_type) << ')';
}
