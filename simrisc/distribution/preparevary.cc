//#define XERR
#include "distribution.ih"

void Distribution::prepareVary()
{
    switch (d_type)
    {
        case NORMAL_VARY:
        break;

        case UNIFORM:
            d_type = UNIFORM_VARY;
        break;

        case LOGNORMAL:
            d_type = LOGNORMAL_VARY;
        break;

        default:
        return;
    }

    Random::instance().use(d_type);

}
