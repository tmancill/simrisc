#define XERR
#include "distribution.ih"

double Distribution::varyProb(double orgValue) const
{
    for (unsigned count = 0; count != MAX_VARY_TRIES; ++count)
    {                                       
        if (
            double ret = varyOK(orgValue); 
            Globals::proportion(ret)
        )
            return ret;
    }

    throw Exception{} << 
        "failed to obtain spreaded proportion for " <<
        orgValue << " (spread = " << d_value << 
                    ", distribution: " << name(d_type) << ')';
}
