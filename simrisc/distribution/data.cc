//#define XERR
#include "distribution.ih"

StringVect Distribution::s_name                     // see enums.h
{
    "Uniform"    ,      // UNIFORM    
    "Uniform"    ,      // UNIFORM_CASE
    "LogNormal"  ,      // LOGNORMAL  

    "Normal",           // NORMAL_VARY
    "Uniform"    ,      // UNIFORM_VARY
    "LogNormal"  ,      // LOGNORMAL_VARY
};

//  "Exponential",      // EXPONENTIAL
