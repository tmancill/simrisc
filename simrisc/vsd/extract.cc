#define XERR
#include "vsd.ih"

// value   spread  dist.   
// 21.1    0.048   Normal  
// 16.51                    // implies spread = 0

istream &VSD::extract(istream &in)
{
    string v1;
    string v2;

    in >> d_value >> d_dist;
    d_orgValue = d_value;
    
    (*d_valueCheck)(d_value);               // check valid SD / Prob. values
    return in;
}


