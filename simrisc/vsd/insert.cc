//#define XERR
#include "vsd.ih"

ostream &VSD::insert(ostream &out) const
{
    return out << setw(8) << d_value << ' ' << 
                  setw(8) << d_dist;
}
