//#define XERR
#include "vsd.ih"

// static
void VSD::proportionCheck(double &value)
{
    if (not Globals::proportion(value))  
    {
        Err::range();
        value = 0;
    }
}




