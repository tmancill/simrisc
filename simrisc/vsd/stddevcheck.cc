//#define XERR
#include "vsd.ih"

// static
void VSD::stdDevCheck(double &value)
{
    if (value < 0)
    {
        Err::msgTxt(Err::NEGATIVE);
        value = 0;
    }
}
