//#define XERR
#include "vsd.ih"

// static
void (*VSD::s_valueCheck[])(double &) =
{
    &meanCheck,
    &stdDevCheck,
    &proportionCheck,
};

// static
VaryType VSD::s_varyType[] =
{
    VARY_OK,
    VARY_POS,
    VARY_PROB
};

