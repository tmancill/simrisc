//#define XERR
#include "simulator.ih"

string Simulator::cmdLineAnalysis()
{
    string specs;
    
    Arg const &arg = Arg::instance();

    for (size_t idx = 0, end = arg.nArgs(); idx != end; ++idx)
    {
        specs += arg[idx];

        if (specs.back() == ',')                // commas end specifications
            specs.back() = '\n';
        else
            specs += ' ';
    }

    d_next = false;

    return specs;
}
