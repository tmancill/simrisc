//#define XERR
#include "simulator.ih"

string Simulator::fileAnalysis()
{
    d_next = false;

    string specs;
    string line;
    while (getline(d_ifstream, line))
    {
        ++d_lineNr;

                                // process all lines until the next
                                // analysis: specification is found
        if (
            size_t pos = line.find_first_not_of(" \t\r");
                pos != string::npos             // found non-blanks 
                and                             // and 'analysis:' as 1st word
                line.find("Analysis:", pos) == pos
        ) 
        {
            d_next = true;
            break;                              // the next analysis is ready
        }

        specs += line + '\n';                   // append the line to the
    }                                           //  specs

    return specs;
}



