#ifndef INCLUDED_AGEGROUPVSD_
#define INCLUDED_AGEGROUPVSD_

#include <iosfwd>
#include "../agegroup/agegroup.h"
#include "../vsd/vsd.h"

class AgeGroupVSD
{
    friend std::istream &operator>>(std::istream &in, AgeGroupVSD &group);
    friend std::ostream &operator<<(std::ostream &out, 
                                                AgeGroupVSD const &group);
    AgeGroup d_ageGroup;

    VSD      d_mean;
    double   d_stdDev;

    public:
        AgeGroup const &ageGroup() const;

        uint16_t beginAge() const;
        uint16_t endAge() const;
        VSD      const &mean() const;
        double          stdDev() const;

        void vary();

    private:
        std::istream &extract(std::istream &in);
        std::ostream &insert(std::ostream &out) const;
};

typedef std::vector<AgeGroupVSD> AgeGroupVSDvect;

inline AgeGroup const &AgeGroupVSD::ageGroup() const
{
    return d_ageGroup;
}

inline uint16_t AgeGroupVSD::beginAge() const
{
    return d_ageGroup.beginAge();
}

inline uint16_t AgeGroupVSD::endAge() const
{
    return d_ageGroup.endAge();
}

inline VSD const &AgeGroupVSD::mean() const
{
    return d_mean;
}

inline double AgeGroupVSD::stdDev() const
{
    return d_stdDev;
}

inline std::istream &operator>>(std::istream &in, AgeGroupVSD &ageGroupVSD)
{
    return ageGroupVSD.extract(in);
}

inline std::ostream &operator<<(std::ostream &out, 
                                            AgeGroupVSD const &ageGroupVSD)
{
    return ageGroupVSD.insert(out);
}

#endif
