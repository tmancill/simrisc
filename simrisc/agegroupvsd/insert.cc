//#define XERR
#include "agegroupvsd.ih"

std::ostream &AgeGroupVSD::insert(std::ostream &out) const
{
    return out << d_ageGroup << " mean: " << d_mean << 
                                ", std.dev: " << d_stdDev;
}
