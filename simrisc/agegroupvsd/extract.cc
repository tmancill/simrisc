//#define XERR
#include "agegroupvsd.ih"

    // e.g.,    1 - 50:  .61    4.38   .43    Normal     

std::istream &AgeGroupVSD::extract(std::istream &in)
{
    d_mean.setMode(VSD::NON_NEGATIVE);
    in >> d_ageGroup  >> d_stdDev >> d_mean;

    // the default Distribution setting is VARY_OK: OK for AgeGroupVSD

    if (d_stdDev < 0)
        Err::msg(Err::NEGATIVE) << "(final std. dev.)" << endl;

    return in;
}

