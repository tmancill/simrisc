//#define XERR
#include "agegroup.ih"

    //  e.g.,   40 - 50:
    //          70 - *:
    // separating blanks are optional

std::istream &AgeGroup::extract(std::istream &in)
{
    char sep;

    in >> d_beginAge >> sep >> sep;     // get '40 - 5', or '70 - *'

    if (sep == '*')
        d_endAge = END_AGE;             // set endAge
    else
    {
        in.unget();                     // or extract the age
        in >> d_endAge;
    }

    return in >> sep;                   // get the final ':' and return
}
