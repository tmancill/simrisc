template <HasAgeGroup Type>
bool AgeGroup::nextRange(std::vector<Type> const &vect) const
{
    return  vect.empty()                    // no Type elements yet
            or
            connects(vect.back().ageGroup());
}
