//#define XERR
#include "agegroup.ih"

bool AgeGroup::operator==(AgeGroup const &other) const
{
    return d_beginAge == other.d_beginAge and d_endAge == other.d_endAge;
}
