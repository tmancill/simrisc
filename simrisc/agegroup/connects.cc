//#define XERR
#include "agegroup.ih"

bool AgeGroup::connects(AgeGroup const &previous) const
{
    if (d_beginAge == previous.d_endAge)
        return true;

    Err::msg(Err::NOT_CONSECUTIVE) << d_beginAge << " - ..." << endl;

    return false;
}
