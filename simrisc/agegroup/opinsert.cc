//#define XERR
#include "agegroup.ih"

std::ostream &operator<<(std::ostream &out, AgeGroup const &ageGroup)
{
    out << setw(3) << ageGroup.d_beginAge << " - " << 
                  setw(2); 

    if (Globals::isZero(ageGroup.d_endAge - END_AGE))
        out << '*';
    else
        out << ageGroup.d_endAge;

    return out << ':';
}
        

