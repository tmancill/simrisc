#ifndef INCLUDED_AGEGROUP_
#define INCLUDED_AGEGROUP_

#include <iosfwd>
#include <ostream>
#include <cstdint>

#include "../typedefs/typedefs.h"
#include "../hasagegroup/hasagegroup.h"

    // no errors, but if the configuration is incorrect then op>> fails

class AgeGroup
{
    friend std::istream &operator>>(std::istream &in, AgeGroup &ageGroup);
    friend std::ostream &operator<<(std::ostream &out, 
                                                AgeGroup const &ageGroup);

    uint16_t    d_beginAge;
    uint16_t    d_endAge;

    public:
        uint16_t beginAge() const;
        uint16_t endAge() const;

            // Err::NOT_CONSECUTIVE if false
        template <HasAgeGroup Type>
        bool nextRange(std::vector<Type> const &vect) const;

        bool operator==(AgeGroup const &other) const;   // we're not promoting

    private:
        std::istream &extract(std::istream &in);
                                    // error message if not connecting
        bool connects(AgeGroup const &previous) const;

};

#include "nextrange.f"

inline uint16_t AgeGroup::beginAge() const
{
    return d_beginAge;
}

inline uint16_t AgeGroup::endAge() const
{
    return d_endAge;
}

inline std::istream &operator>>(std::istream &in, AgeGroup &ageGroup)
{
    return ageGroup.extract(in);
}

inline bool operator!=(AgeGroup const &lhs, AgeGroup const &rhs)
{
    return not (lhs == rhs);
}

#endif
