//#define XERR
#include "configlines.ih"

ConfigLines::ConfigLines(istream &in, uint16_t lineNr)
:
    d_config(in, lineNr)
{
    d_iter = d_config.begin();
}
