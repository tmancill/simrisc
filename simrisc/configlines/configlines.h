#ifndef INCLUDED_CONFIGLINES_
#define INCLUDED_CONFIGLINES_

#include <fstream>
#include <bobcat/config>

#include "../options/options.h"

class ConfigLines
{
    typedef FBB::Config::const_iterator     const_iterator;
    typedef FBB::CF_Line                    CF_Line;

    FBB::Config d_config;

    uint16_t d_lineNr;
    std::string d_key;
    std::string d_tail;
    std::string d_line;
    std::string d_value;

    const_iterator d_iter;                  // iterator to Config lines
    bool d_redo = false;                    // next get() returns d_line

    public:
        ConfigLines(std::string const &fname);
        ConfigLines(std::istream &in, uint16_t lineNr);

//        void load();

        void redo();

        bool get();

        std::string const &line() const;    // only after get() returns true
        std::string const &tail() const;         
        std::string const &value() const;   // 1st word of the key's value

        std::string const &key() const;
        uint16_t lineNr() const;

    private:
};

inline uint16_t ConfigLines::lineNr() const
{
    return d_lineNr;
}

inline void ConfigLines::redo()
{
    d_redo = true;
}

inline std::string const &ConfigLines::key() const
{
    return d_key;
}
        
inline std::string const &ConfigLines::line() const
{
    return d_line;
}
        
inline std::string const &ConfigLines::value() const
{
    return d_value;
}
        
inline std::string const &ConfigLines::tail() const
{
    return d_tail;
}
        
#endif
