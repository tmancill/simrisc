//#define XERR
#include "specificity.ih"

bool Specificity::operator==(Specificity const &other) const
{
    return d_value == other.d_value and d_ageGroup == other.d_ageGroup;
}
