#ifndef INCLUDED_SPECIFICITY_
#define INCLUDED_SPECIFICITY_

#include <iosfwd>
#include <vector>

#include "../agegroup/agegroup.h"

class Specificity
{
    friend std::istream &operator>>(std::istream &in, Specificity &spec);

    AgeGroup d_ageGroup;
    double d_value;

    public:
        AgeGroup const &ageGroup() const;
        uint16_t beginAge() const;
        uint16_t endAge() const;
        double   value() const;

        bool proportion();
              
                                                            // we're not 
        bool operator==(Specificity const &other) const;    // promoting

    private:
        std::istream &extract(std::istream &in);
};

typedef std::vector<Specificity> SpecificityVect;

inline AgeGroup const &Specificity::ageGroup() const
{
    return d_ageGroup;
}
        
inline uint16_t Specificity::beginAge() const
{
    return d_ageGroup.beginAge();
}

inline uint16_t Specificity::endAge() const
{
    return d_ageGroup.endAge();
}

inline double Specificity::value() const
{
    return d_value;
}

inline std::istream &operator>>(std::istream &in, Specificity &spec)
{
    return spec.extract(in);
}

inline bool operator!=(Specificity const &lhs, Specificity const &rhs)
{
    return not (lhs == rhs);
}



#endif
