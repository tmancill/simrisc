//#define XERR
#include "specificity.ih"

bool Specificity::proportion()
{
    if (Globals::proportion(d_value))
        return true;

    Err::range();

    return false;
    
}
