//#define XERR
#include "specificity.ih"

istream &Specificity::extract(istream &in)
{
    return in >> d_ageGroup >> d_value;
}
