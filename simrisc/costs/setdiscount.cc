//#define XERR
#include "costs.ih"

void Costs::setDiscount()
{
    d_base.back() = "Discount:";
    d_base.resize(d_base.size() + 1);

    setAge();
    setProp();

    d_base.resize(d_base.size() - 1);
}
