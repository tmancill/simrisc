//#define XERR
#include "costs.ih"

uint16_t Costs::cost(double diameter) const
{
                    // reversed search: start at the max. diameter
    return find_if(d_treatment.rbegin(), d_treatment.rend(),
                [&](CostPair const &costPair)
                {
                    return diameter >= costPair.first;
                }
            )->second;
}

//    xerr("cost with diam. " << diameter << " = " << ret);
