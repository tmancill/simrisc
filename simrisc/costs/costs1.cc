//#define XERR
#include "costs.ih"

Costs::Costs()
:
    d_base{ "Costs:", "" }
{
    setBiop();
    setDiameters();
    setDiscount();
}

