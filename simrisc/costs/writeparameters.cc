//#define XERR
#include "costs.ih"

void Costs::writeParameters(ostream &out) const
{
    Globals::setPrecision(out, 1) <<
        "Costs:\n"
        "  biopsy:          " << setw(4) << d_biop << "\n"
        "  Discount:\n"
        "    reference age: " << setw(4) << d_referenceAge << "\n"
        "    proportion:    " << setw(4) << d_discountProportion << "\n"
        "  diameters:\n";
    
    for (size_t idx = 0; idx != d_treatment.size(); ++idx)
        out << 
        "    diameter: >= " << 
                setw(6) << d_treatment[idx].first << " mm: " <<
                setw(4) << d_treatment[idx].second << '\n';

    Globals::setPrecision(out, 3) << '\n';
}
