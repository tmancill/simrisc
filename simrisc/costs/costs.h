#ifndef INCLUDED_COSTS_
#define INCLUDED_COSTS_

// Costs:
//     biop:       176   
//     diameters:  0: 6438  20: 7128  50: 7701
//     Discount:
//         age:        50      
//         prop:       0


#include "../typedefs/typedefs.h"
#include "../parser/parser.h"

class Costs
{
              // lower diam., cost
    typedef std::pair<double, uint16_t>     CostPair;

    StringVect d_base;
                    
                                            // costs of treatments given
    std::vector<CostPair> d_treatment;      // tumor diam.
 
    double d_referenceAge;                  // discount reference age
    double d_discountProportion;

    uint16_t d_biop;                        // biopsy cost

    public:
        Costs();

        double biopsy(double age) const;                            // .h
        double screening(double age, uint16_t cost) const;          // .h
        double treatment(double age, double diameter) const;        // .h

        void writeParameters(std::ostream &out) const;

    private:
        void setBiop();
        void setDiameters();
        bool extractDiameters(Parser::Lines &&lines);
        void setDiscount();
        void setAge();
        void setProp();

        uint16_t cost(double diameter) const;
        double discount(double age, size_t cost) const;
};


inline double Costs::biopsy(double age) const
{
    return discount(age, d_biop);
}

inline double Costs::screening(double age, uint16_t cost) const
{
    return discount(age, cost);
}

inline double Costs::treatment(double age, double diameter) const
{
    return discount(age, biopsy(age) + cost(diameter));
}

#endif

