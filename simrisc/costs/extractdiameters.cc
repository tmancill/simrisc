#define XERR
#include "costs.ih"

bool Costs::extractDiameters(Parser::Lines &&lines)
{
    if (not lines)
        return false;

    istringstream in{ lines.get()->tail };      // extract the diameters
    double lastAge = -1;                        // ages must increase

    CostPair spec;
    while (in >> spec.first)                    // begin diameter
    {
        if (spec.first <= lastAge)              // not increasing
            return false;

                                                // ignore the :
        if (not (in.ignore() >> spec.second))   // followed by treatment
            return false;                       // cost. Ends at an
                                                // incomplete entry
        d_treatment.push_back(spec);
        lastAge = spec.first;
    }

    return  d_treatment.size() > 0 
            and 
            Globals::isPositiveZero(d_treatment.front().first);
}

