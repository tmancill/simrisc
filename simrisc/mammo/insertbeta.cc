//#define XERR
#include "mammo.ih"

void Mammo::insertBeta(ostream &out) const
{
    out << "    beta parameters: ";

    Globals::setPrecision(out, 2);

    for (unsigned idx = 0; idx != N_BETA; ++idx)
        out << idx << ": " << d_beta[idx] << ", ";

    out.put('\n');
}
