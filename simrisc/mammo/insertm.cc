//#define XERR
#include "mammo.ih"

void Mammo::insertM(ostream &out) const
{
    out << "    m parameters:    ";

    Globals::setPrecision(out, 3);

    for (size_t idx = 0; idx != N_BETA; ++idx)
        out << static_cast<char>('a' + idx) << ": " << 
                                            setw(5) <<d_m[idx] << ", ";
    
    out.put('\n');

}
