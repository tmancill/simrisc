//#define XERR
#include "mammo.ih"

void Mammo::setM()
{
    d_base.back() = "m:";

    if (Parser::extract(Parser::one(d_base), d_m, N_BIRADS))
        Globals::proportions(d_m, N_BIRADS);
}
