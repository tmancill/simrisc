#define XERR
#include "mammo.ih"

void Mammo::vInsert(ostream &out) const
{
    insertBirads(Globals::setPrecision(out, 1), "dose:     ", d_dose);

    insertM(out);
    insertBeta(out);
    insertSpecificity(Globals::setPrecision(out, 3), 5, d_specVect);

    Globals::setPrecision(out, 2) << 
                setw(4) << ' ' << "systematicError:  " << d_sysErr << '\n';

}
