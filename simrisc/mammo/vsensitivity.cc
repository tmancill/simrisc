#define XERR
#include "mammo.ih"

//code
// cf. Isheden, G., & Humphreys, K. (2017). Modelling breast cancer tumour
// growth for a stable disease population. Statistical Methods in Medical
// Research, 28(3), 681-702.

double Mammo::vSensitivity(size_t idx) const
{
    double diameter = d_tumor.diameter();
    double mValue = d_m[idx];

    double expBeta = 
        exp(
            d_beta[0]            +
            d_beta[1] * diameter +
            d_beta[2] * mValue   +
            d_beta[3] * mValue / (diameter * diameter)
        );
  
    return  (1 - d_sysErr) * expBeta / (1 + expBeta);

}
//=
