#ifndef INCLUDED_MAMMO_
#define INCLUDED_MAMMO_

#include "../enums/enums.h"
#include "../modbase/modbase.h"
#include "../specificity/specificity.h"

//Modalities:
//
//  Mammo:
//      costs:          64   
//
//      #      bi-rad:  a       b       c       d             
//      dose:           3       3       3       3  
//      m:             .136    .136    .136    .136
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965
//
//      #       1       2       3        4    
//      beta:  -4.38    .49     -1.34    -7.18
//
//      systematicError:  0.1


class Tumor;

class Mammo: public ModBase
{
    enum BetaParams
    {
        N_BETA = 4
    };

    Tumor const &d_tumor;

    StringVect d_base;

    double d_dose[N_BIRADS];
    double d_m[N_BIRADS];
    SpecificityVect d_specVect;

    double d_beta[N_BETA];
    double d_sysErr;

    public:
        Mammo(Tumor const &tumor);
        ~Mammo() override;

    private:
        void doseHandler();
        void betaHandler();
        void setSystematicError();
        void setM();

        void insertBeta(std::ostream &out) const;
        void insertM(std::ostream &out) const;

        double const *vDose() const override;               // 1
        double vDose(uint16_t) const override;              // 2
        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;

};

#endif
