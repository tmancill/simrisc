//#define XERR
#include "mammo.ih"

//Modalities:
//
//  Mammo:
//      costs:          64   
//
//      #      bi-rad:  a       b       c       d             
//      dose:           3       3       3       3  
//      m:             .136    .136    .136    .136
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965
//
//      #       1      2   3      4      
//      beta:  -4.38  .49  -1.34  -7.18  
//
//      #                default:
//      # systematicError:  0.1 

Mammo::Mammo(Tumor const &tumor)
:
    ModBase("Mammo"),
    d_tumor(tumor),
    d_base{ "Modalities:", "Mammo:", "" }
{
    costHandler(d_base);                        // ModBase 
    doseHandler();
    setM();
    specificityHandler(d_specVect, d_base);     // ModBase
    betaHandler();
    setSystematicError();
}





