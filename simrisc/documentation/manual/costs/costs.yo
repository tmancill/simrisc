The class tt(Costs) computes treatment, screening and biopsy costs given age
and other arguments that specifically apply to the cost-type.

The class's constructor reads the costs specifications from its tt(Scenario)
argument. The specifications are formatted like this:
    verb(
#         discount:             pairs of (increasing) begin-diam: cost
#         age   cost    biop    (first diameter must be 0)
costs:    50    0       176     0: 6438  20: 7128  50: 7701
    )

Its tt(d_treatment) vector stores the various diameter/costs combinations. The
age specifications are tt(double) values. 

Biopsy, screening and treatment costs are all computed by the member
tt(discount). Depending on the context (cf. section ref(LOOP)) either the
screening age or the tumor self-detection age is used as tt(discount's) 
age argument. 

When computing the biopsy cost the configured biopsy cost is used as
tt(discount's) cost argument. When computing the screening costs the costs of
the used modality are used. When computing the treatment costs the biopsy
costs plus the costs associated with the tumor's diameter are used. 

The member tt(discount) is a simple 1-line function:
    verbinsert(-s4 //code src/discount.cc)

The costs associated with the tumor's diameter is returned by the member
tt(cost), returning the costs that are associated with the last configured
diameter that is at least equal to the specified diameter (e.g., if the
diameters 0, 20, and 50 are configured and the specified diameter is equal to
30 then the costs associated with diameter 20 are used.

