//#define XERR
#include "tumor.ih"

//  (ORG: calculateSize)
void Tumor::characteristics(double age)
{
    check("size");
  
    if ((d_present = (d_detectableAge <= age)))            // there is a tumor
    {
        d_diameter = 
            diameter(
                d_volume = d_startVolume *          // exp. growth of volume 
                           pow(2, (age - d_detectableAge) / d_doublingYears)
            );
    }
    else 
    {
        d_volume = 0;
        d_diameter = 0;
    }
}





