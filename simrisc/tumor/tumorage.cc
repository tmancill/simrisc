//#define XERR
#include "tumor.ih"

//    if (0.120662 < rndTumor and rndTumor < 0.120664)
//    {
//        cerr.setf(ios::fixed, ios::floatfield);
//
//        for (double value: d_tumorInfo.cumTotalRisk())
//            cerr << setw(6) << value << '\n';
//    }


bool Tumor::tumorAge()
{
    Random &random = Random::instance();

    d_age = findAge(d_tumorInfo.cumTotalRisk(), random.uniformCase());

    xerr("random tumor prob: " << rndTumor << ", age " << d_age);

    Options::instance().fixedTumorAge(d_age);

    if (d_age <= MAX_AGE)
        return true;

    d_age = NO_TUMOR;

    random.uniformCase();             // extra calls to synchronize the
    random.logNormal(0, 0);           // random generator between 
    random.logNormal(0, 0);           // tumor and no tumor states

    return false;
}
