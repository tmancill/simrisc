#define XERR
#include "tumor.ih"

    // this function computes the estimated death age using the function 
    // published in
    // Cancer. 2002 Aug 15;95(4):713-23.  Predicting the survival of patients
    // with breast carcinoma using tumor size.  Michaelson JS, Silverstein M,
    // Wyatt J, Weber G, Moore R, Halpern E, Kopans DB, Hughes K.

void Tumor::setDeathAge(double detectionAge)
{
    check("death age");

    // interval halving: search between 0 and 100
    double low = 0; 
    double high = 100;
    double mid;
    for (size_t iter = 0; iter != N_ITER_AGE; ++iter) 
    {
        mid = (low + high) / 2;       
        
        switch (weakCompare(functionF(mid), d_pSurvival))
        {
            case -1:                // use the lhs half: low..mid
                high = mid;
            continue;

            case 1:                 // use the rhs half: mid::high
                low = mid;
            continue;

            default:                // equal within WEAK_TOLERANCE
            break;
        }

        break;                      // convergence 
    }

    d_deathAge = std::min(detectionAge + mid, 100.);
}
