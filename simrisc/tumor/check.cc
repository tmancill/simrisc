//#define XERR
#include "tumor.ih"

void Tumor::check(char const *type) const
{
    if (d_age == NO_TUMOR)
        throw Exception{} << "cannot determine tumor " << type << 
                             " for a non-existing tumor";
}
