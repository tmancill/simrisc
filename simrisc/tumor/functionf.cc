//#define XERR
#include "tumor.ih"

double Tumor::functionF(double years) const
{
    return years <= 0 ? 
                1 
            : 
                exp(-functionQ(years) * pow(d_diameter, functionZ(years))); 
}
        
