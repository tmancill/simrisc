#define XERR
#include "tumor.ih"

Tumor::Tumor(TumorInfo const &tumorInfo)
:
    d_tumorInfo(tumorInfo),
    d_growth(tumorInfo.growth()),
    d_survival(tumorInfo.survival())
{}



