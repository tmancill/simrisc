//#define XERR
#include "tumor.ih"

double Tumor::volume() const
{
    check("volume");
    return d_volume;
}

