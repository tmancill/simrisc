//#define XERR
#include "tumor.ih"

void Tumor::reset()
{
    d_detected = false;
    d_interval = false;

    d_present = false;                  // see 'characteristics()'
    d_volume = 0;
    d_diameter = 0;
    d_deathAge = 0;

    if (not tumorAge())                 // sets doublingTime
    {
        d_pSurvival = 1;
        d_startVolume = 0;              // see modifications (7)
        d_doublingDays = NO_TUMOR;
        d_doublingYears = NO_TUMOR;
        d_selfDetect = 0;
        d_prePeriod = NO_TUMOR;
        d_detectableAge = NO_TUMOR;
        return;
    }

    uint16_t idx = ageGroup();

    Random &random = Random::instance();

    d_pSurvival = random.uniformCase();

    d_startVolume = volume(d_growth.start());

    AgeGroupVSD const &group = d_growth.ageGroupVSD(idx);
    d_doublingDays = random.logNormal(group.mean().value(), group.stdDev());

    d_doublingYears = d_doublingDays / N_YEARDAYS;

    d_selfDetect = random.logNormal(d_growth.selfMu().value(), 
                                    d_growth.selfSigma());

    d_prePeriod =   d_doublingDays *
                    log(
                        volume(d_selfDetect) / 
                        volume(d_growth.start())
                    ) * s_log2YearInv;

    d_detectableAge = d_age - d_prePeriod;
}



