//#define XERR
#include "tumor.ih"

double Tumor::functionQ(double years) const
{
    return d_survival[0].value() * pow(years, d_survival[1].value());
}
