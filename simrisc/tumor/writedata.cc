//#define XERR
#include "tumor.ih"

void Tumor::writeData(CSVTable &tab) const
{
    tab.more() << s_noYes[d_present] <<                             // 4
                  s_noYes[d_detected] <<                            // 5
                  s_noYes[d_interval];                              // 6

            // see modifications (8)
    if (d_diameter > 1000)                                          // 7
        tab.more() << "1001";                   // was: 'inf'
    else
        tab.more() << d_diameter;

            // see also modifications (7)
            // //setw(6) << d_age == NO_TUMOR ? 0 : age << '\t' <<

    tab.more() << d_doublingDays << d_prePeriod <<                  // 8 - 9
                  d_detectableAge << d_age << d_deathAge;           // 10 - 12

}






