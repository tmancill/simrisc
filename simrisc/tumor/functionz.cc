//#define XERR
#include "tumor.ih"

double Tumor::functionZ(double years) const
{
    return d_survival[2].value() * log(years) 
           + d_survival[3].value();
}
        
