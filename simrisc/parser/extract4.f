// static
template <Extractable Type, Extractable ...Types>
bool Parser::extract(std::istream &in, Type &first, Types &...more)
{
    in >> first;
    return extract(in,  std::forward<Types &>(more)...);
}

inline bool Parser::extract(std::istream &in)
{
    return static_cast<bool>(in);
}

