//#define XERR
#include "parser.ih"

// static
void Parser::addConfigLine(vector<Map *> &mapPtrVect, 
                           ConfigLines &lines, ParamsSrc src)
{
    string key = lines.key();

    Map &map = *mapPtrVect.back();          // look for the key in the current
    auto iter = map.find(key);              // map section

    if (iter != map.end())                  // key was found
    {
        if (endPoint(*iter))                // no deeper level: store the line
            iter->second->d_lines.back().push_back
            ( 
                { src, lines.lineNr(), lines.line(), lines.tail() } 
            );
        else                            // key found, but not an endpoint:
            mapPtrVect.push_back(&iter->second->d_map);  // go nesting

        return;
    }

    locateError(mapPtrVect, lines, src);
}




