// static
template <Extractable Type>
bool Parser::one(StringVect const &base, Type &dest)
{
    return static_cast<bool>(extract(one(base), dest));
}
        
