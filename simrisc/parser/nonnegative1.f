inline bool Parser::nonNegative(StringVect const &base, double &dest)
{
    return atLeast(0, base, dest);
}
