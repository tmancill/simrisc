//#define XERR
#include "parser.ih"

// static
std::vector<LineInfoVect> const &Parser::keywordsLines(
                                            StringVect const &keywords)
{
    Map const *mapPtr = &s_map;
    Map::const_iterator mapIter;

    for (auto const &keyword: keywords)         // visit the range of keywords
    {
        mapIter = mapPtr->find(keyword);        // ptr to Map::value_type

        if (mapIter == mapPtr->end())       // internal error: throw exception
            throw Exception{} << "Internal error: keyword `" << keyword << 
                                 "' not configured";

        mapPtr = &mapIter->second->d_map;       // use the next keyword
    }

                                                // return the first used 
    return mapIter->second->d_lines;            // LineInfoVect
}
