//#define XERR
#include "parser.ih"

void Parser::load(std::istream &in, uint16_t startLineNr,   
                  StringVect &labels, 
                  OptionsVect &options)
{
    ConfigLines lines{ in, startLineNr };   // read the current stream

    setLabels(lines, labels);
    setOptions(lines, options);

    addLineInfoVectors(s_map);              // add (empty) LineInfoVects at
                                            // the map's deepest nesting 
                                            // levels

    vector<Map *> mapPtrVect{ &s_map };     // start loading at the top

    while (lines.get())                     // add the config lines
        addConfigLine(mapPtrVect, lines, ANALYSIS);
}
