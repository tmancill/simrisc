inline Parser::Lines::operator bool() const
{
    return d_size != 0;
}
