//#define XERR
#include "parser.ih"

    // In the configuration file specifications are just lines. Each keyword 
    // must therefore be searched via a unique path. So searching for
    // e.g., { 'costs:' } returns multiple hits, but searching for 
    // { 'Scenario:', 'costs:' } returns only one hit

namespace {


Parser::MapValue scenario
{
    "Scenario:", 
    new Parser 
    {
        { // map
            {"spread:",     new Parser },
            {"iterations:", new Parser },
            {"generator:",  new Parser },
            {"seed:",       new Parser },
            {"cases:",      new Parser },
        }
    }
};

Parser::MapValue screening
{
    "Screening:", 
    new Parser 
    {
        { // map
            {"round:",              new Parser },
            {"attendanceRate:",     new Parser },
        }
    }
};

Parser::MapValue breastDensities
{
    "BreastDensities:", 
    new Parser 
    {
        { // map
            {"ageGroup:", new Parser},
        }
    }
};

Parser::MapValue mammo
{
    "Mammo:",
    new Parser
    {
        {
            {"costs:",              new Parser },
            {"dose:",               new Parser },
            {"m:",                  new Parser },
            {"specificity:",        new Parser },
            {"beta:",               new Parser },
            {"systematicError:",    new Parser },
        }
    }
};

Parser::MapValue tomo
{
    "Tomo:",
    new Parser
    {
        {
            {"costs:",          new Parser },
            {"dose:",           new Parser },
            {"specificity:",    new Parser },
            {"sensitivity:",    new Parser },
        }
    }
};

Parser::MapValue mri
{
    "MRI:",
    new Parser
    {
        {
            {"costs:",          new Parser },
            {"specificity:",    new Parser },
            {"sensitivity:",    new Parser },
        }
    }
};

Parser::MapValue modalities
{
    "Modalities:", 
    new Parser
    {
        { // map
            mammo,
            tomo,
            mri
        }
    }
};


Parser::MapValue normal
{
    "Normal:",
    new Parser
    {
        {
            {"probability:", new Parser },
            {"lifetimeRisk:", new Parser },
            {"meanAge:", new Parser },
            {"stdDev:", new Parser },
        }
    }
};

Parser::MapValue brca1
{
    "BRCA1:",
    new Parser
    {
        {
            {"probability:", new Parser },
            {"lifetimeRisk:", new Parser },
            {"meanAge:", new Parser },
            {"stdDev:", new Parser },
        }
    }
};

Parser::MapValue brca2
{
    "BRCA2:",
    new Parser
    {
        {
            {"probability:", new Parser },
            {"lifetimeRisk:", new Parser },
            {"meanAge:", new Parser },
            {"stdDev:", new Parser },
        }
    }
};

Parser::MapValue incidence
{
    "Incidence:",
    new Parser
    {
        {
            normal,
            brca1,
            brca2,
        }
    }
};

Parser::MapValue doublingTime
{
    "DoublingTime:",
    new Parser
    {
        {
            {"ageGroup:", new Parser},
        }
    }
};

Parser::MapValue growth
{
    "Growth:",
    new Parser
    {
        {
            {"start:", new Parser },
            {"selfDetect:", new Parser },
            doublingTime,
        }
    }
};

Parser::MapValue survival
{
    "Survival:",
    new Parser
    {
        {
            {"type:", new Parser },
        }
    }
};

Parser::MapValue tumor
{
    "Tumor:", 
    new Parser
    {
        { // map
            incidence,
            growth,
            { "beir7:",     new Parser },
            survival,
        }
    }
};

Parser::MapValue discount
{
    "Discount:", 
    new Parser
    {
        { // map
            {"age:",        new Parser },
            {"proportion:", new Parser },
        }
    }
};

Parser::MapValue costs
{
    "Costs:", 
    new Parser
    {
        {
            {"biop:",       new Parser },
            {"diameters:",  new Parser },
            discount,
        }
    }
};

} // namespace

Parser::Map Parser::s_map
{
    scenario,
    costs,
    screening,
    breastDensities,
    modalities,
    tumor,
};
