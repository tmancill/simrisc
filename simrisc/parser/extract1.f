// static
template <Extractable ...Types>
std::istringstream Parser::extract(Lines &&lines, Types &...dest)
{
    if (lines)
        return extract(*lines.get(), std::forward<Types &>(dest) ...);
    
    std::istringstream in;
    in.setstate(std::ios::eofbit);
    return in;
}
        
