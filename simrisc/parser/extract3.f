// static
template <Extractable Type>
std::istringstream Parser::extract(Lines &&lines, Type *dest, size_t size)
{
    std::istringstream in;

    if (not lines)
    {
        in.setstate(std::ios::eofbit);
        return in;
    }

    in.str(lines.get()->tail);

    for (; size--; ++dest)          // fill 'size' elements starting at dest
    {
        if (not (in >> *dest))
        {
            in.setstate(std::ios::failbit);
            Err::specification();
            break;
        }
    }

    return in;
}
        
