// static
inline bool Parser::positive(StringVect const &base, double &dest)
{
    return atLeast(Globals::WEAK_TOLERANCE, base, dest);
}
