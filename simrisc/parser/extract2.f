// static
template <Extractable ...Types>
std::istringstream Parser::extract(LineInfo const &line, Types &...dest)
{
    std::istringstream in{ line.tail };
    
    if (not extract(in, std::forward<Types &>(dest) ...))
    {
        in.setstate(std::ios::failbit);
        Err::specification();
    }

    return in;
}
        
