//#define XERR
#include "main.ih"

// needs parser, parserlines and error

int main(int argc, char **argv)
try
{
    if (argc == 1)
        throw Exception{} << "arg1: simrisc conf., arg2: one analysis spec\n";

    Parser parser;

    StringVect labels;
    Parser::OptionsVect options;

    ifstream in{ argv[2] };
    parser.load(in, 1, labels, options);
    parser.load(argv[1]);

    cout << "Labels:\n";
    for (string const &label: labels)
        cout << "   Label: " << label << '\n';

    cout << "Options:\n";
    for (auto const &option: options)
        cout << "   Option: `" << option.name << "': " << 
                                 option.value << '\n';


    cout << "Spread:\n";
    for (auto element: parser.select({"Scenario:", "spread:"}))
        cout << "src: " << element.src << ", nr: " << element.lineNr << 
                ", tail: " << element.tail << '\n';

    cout << "nCases:\n";
    for (auto element: parser.select({"Scenario:", "nCases:"}))
        cout << "src: " << element.src << ", nr: " << element.lineNr << 
                ", tail: " << element.tail << '\n';

    {
        cout << "Costs:\n";
        LineInfoVect const &lines = Parser::select({ "Costs:" });
    
        if (lines.front().tail.empty())
            Err::msg(Err::UNDEFINED_SPEC) << ": Costs" << endl;
        else 
        {    
            double refAge;
            double prop;
            uint16_t biop;

            std::istringstream in{ 
                                    Parser::extract(
                                        lines[0], refAge, prop, biop
                                    )
                                };
            if (in)
                cout << refAge << ", " << biop << '\n';
        }
    }
}
catch(int x)
{
    return x;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
