template <std::integral Type> 
bool Parser::nonNegative(StringVect const &base, Type &dest)
{
    double tmp;
    bool ret = nonNegative(base, tmp);
    dest = tmp;
    return ret;
}
