//#define XERR
#include "parser.ih"

// static
bool Parser::atLeast(double minimum, StringVect const &base, double &dest)
{
    if (extract(one(base), dest))
    {
        if (minimum <= dest)
            return true;

        Err::atLeast(minimum);
    }

    dest = minimum;
    return false;
}
