//#define XERR
#include "parser.ih"

// static
void Parser::setOptions(ConfigLines &parser, OptionsVect &options)
{
    while 
    (
        parser.get()                                // got a line and it's not
        and                                         // a top-level parser
        s_map.find(parser.key()) == s_map.end()     // parameter
    )
        options.push_back( 
                    { 
                        parser.key().back() == ':' ?
                            parser.key().substr(0, parser.key().length() - 1)
                        :
                            parser.key(),
                        parser.value(),
                        parser.lineNr(),
                    } 
                );

    parser.redo();             // redo the line not containing a label
}
