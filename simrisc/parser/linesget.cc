#define XERR
#include "parser.ih"

LineInfo const *Parser::Lines::get()
{
    return d_iter == d_end ?               // no more lines
                0
            :
                Err::reset(*d_iter++);  // allow new error messages, provide
                                        // Err with the current line and 
                                        // return the current line's address
}
