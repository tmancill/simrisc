//#define XERR
#include "parser.ih"

// static
Parser::Lines Parser::lines(StringVect const &keywords)
{
    // Map is unordered_map<std::string, Parser *>

    auto const &vectors = keywordsLines(keywords);

    for (auto const &vect: vectors)     // multiple vectors are possible
    {                                   // because of respecifications in
        if (vect.size() != 0)           // extra analysis-specs. Return
{
xerr("      line[0]: " << vect.front().txt);
            return Lines{ vect.begin(), vect.end() };
}

    }

    return Lines{ vectors.front() };    // initializes to empty: no lines
}






