#define XERR
#include "parser.ih"

// static
void Parser::locateError(vector<Map *> &mapPtrVect, 
                          ConfigLines &lines, ParamsSrc src)
{
    string key = lines.key();                   // look for this key

    for (size_t idx = mapPtrVect.size(); idx--; )   // upwards from the most
    {                                               // nested level
        Map &map = *mapPtrVect[idx];
                                                // found the key at an earlier
        if (map.find(key) != map.end())         // level
        {
            mapPtrVect.resize(idx + 1);         // remove nested pointers
            lines.redo();                       // reprocess this line
            return;
        }
    }

    // the key at the current line was not found in this section

    emsg << Err::src(src) << " [" << lines.lineNr() << "]: ";
    Err::msg(Err::UNDEFINED_SPEC) << " `" << lines.line() << '\'' << endl;
}



