#define XERR
#include "parser.ih"

// static
void Parser::addLineInfoVectors(Map &map)
{
        // element.first: the key
        // element.second: Parser *

    for (auto &element: map)                    // all elements of 'map'
    {
        if (not endPoint(element))              // there's a deeper link:
            addLineInfoVectors(element.second->d_map);  //      continue there

        else                                    // no map is used here
        {
            auto &vect = element.second->d_lines;   // add a LineInfo vector
            vect.resize(vect.size() + 1);           // of lines
        }
    }
}
