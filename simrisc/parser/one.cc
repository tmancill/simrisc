//#define XERR
#include "parser.ih"

// static
Parser::Lines Parser::one(StringVect const &keywords)
{
    Lines lines = any(keywords);

    if (lines.size() > 1)
        Err::multiplySpecified(lines.begin(), lines.end(), 
                               sectionList(keywords));

    return lines;
}
