//#define XERR
#include "parser.ih"

void Parser::load(string const &fname)
{
    ConfigLines lines{ fname };

    addLineInfoVectors(s_map);              // add (empty) LineInfoVects at
                                            // the map's deepest nesting 
                                            // levels

    vector<Map *> mapPtrVect{ &s_map };     // start loading at the top

    while (lines.get())                   // add the parser lines
        addConfigLine(mapPtrVect, lines, CONFIGFILE);
}
