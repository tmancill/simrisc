Simrisc is a simulation program that is used for simulating the consequences 
(death risk, costs, recovery, treatment effects, etc.) of breast cancer. 
The original version of this program was written around 2010 by Marcel Greuter,
and updated in 2015 by Chris de Jonge.
The version in this repository is a complete rewrite of those previous versions.